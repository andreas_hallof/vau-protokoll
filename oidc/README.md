# Authentisierung bei ePA

Die Authentisierung bei ePA-3.x unterteilt sich in zwei Abschnitte

1. Über die Etablierung eines VAU-Kanals werden zwischen LEI und ePA-VAU
   symmetrische Verbindungsschlüssel ausgetauscht. Eine LEI kann sich nach
   erfolgreicher Schlüsselaushandlung und der Prüfung der dabei auftretenden
   Artefakte (AUT-Zertifikat der ePA-VAU, signierte VAU-Schlüssel)
   sich sicher sein, dass es eine frische Verbindung zu einer ePA-VAU hat.
   (frischer, vertraulicher und einseitig authentisierter Kanal)

2. direkt dannach kann sich eine ePA-VAU noch nicht sicher sein (mit dem VAU-NP
   hat es nur eine gute Vermutung) wer der Client ist. Deshab muss über den
   VAU-Kanal eine Nutzerauthentisierung der LEI über OIDC/PKCE geschehen.
   Hat dies erfolgreich stattgefunden, d. h. die ePA-VAU hat vom IDP ein
   ID-Token erhalten, so weiß die ePA-VAU jetzt sicher um welche LEI es sich 
   handelt. Die beidseitige Authentsierung ist jetzt abgeschlossen. Erst ab
   jetzt läßt die ePA-VAU epa-fachliche Befehle über den VAU-Kanal zu.


# Übersichtsdiagramme des Authentication-Flows einer LEI in der Rolle ePA-Client

## Übersichtsdiagramm 1
![OIDC-Übersicht-Graph-1](graphs/oidc-lei-1.png)

## Übersichtsdiagramm 2
![OIDC-Übersicht-Graph-2](graphs/oidc-lei-2.png)

## Übersichtsdiagramm 3
![OIDC-Übersicht-Graph-3](graphs/oidc-lei-3.png)

# Beschreibung


1.	"Das Anwendungsfrontend überträgt seine Initiale Zugriffsanfrage an den
    Fachdienst. Idealerweise als OAuth2 Request"

2.	"Der Authorisierungsserver des Fachdienstes erzeugt sich einen zufälligen
    Code Verifier und bildet darüber den Hash Code Challenge mit dem
    Hash-Algorithmus S256. Dann formuliert er den vollständigen OpenID
    Authorization Request an den IDP-Dienst. (Optional erzeugt er sich
    zusätzlich eine zufällige nonce)"

3.	"Der Authorisierungsserver des Fachdienstes antwortet auf die Initiale
    Zugriffsanfrage des Anwendungsfrontend mit den notwendigen Parametern um
    einen vollständigen Authorization Request an den Authorization-Endpunkt des
    IDP-Dienstes zu stellen. (hier bietet sich ein 302 Redirect an)"

4.	"Das Anwendungssystem (PVS/KIS) ruft sein Authenticator-Modul (je nach
    technischer Umsetzung) auf."

5.	"Das Authenticator-Modul überträgt den Authorization Request inklusive der
    Code Challenge weiter an den Authorization-Endpunkt des IDP-Dienstes."

6.	"(a) Der Authorization-Endpunkt stellt den einmalig den Consent (Zustimmung
    des Nutzers zur Verarbeitung der angezeigten Daten) zu den mit dem
    entsprechenden Fachdienst vereinbarten Claims zusammen und überträgt
    Challenge-Token und Consent-Abfrage User-Consent zum Authenticator-Modul.
    (b) Das Authenticator-Modul fordert den Nutzer einmalig zur
    Consent-Freigabe auf mittels Smartcard und PIN-Eingabe und verwendet die
    PIN, um die Challenge-Token von der Smartcard signieren zu lassen. Das
    Authenticator-Modul überträgt dann das signierte Challenge-Token mit dem
    Smartcard-Zertifikat, verschlüsselt mittels `PuK_IDP_ENC`, an den IDP-Dienst.
    (c) Der IDP-Dienst prüft das Smartcard-Zertifikat des Nutzers gegen
    OCSP/TSL-Dienst."

7.	"Der Authorization-Endpunkt entschlüsselt und validiert die signierte
    Challenge Session-ID, Challenge und Signatur. Die Signatur wird anhand des
    im ""x5c""-Header mitgelieferten Authentifizierungszertifikats der
    Smartcard validiert. Die Validierung des Zertifikats erfolgt durch Prüfung
    gegen OCSP/TSL-Dienst der gematik-PKI.  Der Authorization-Endpunkt erstellt
    den Authorization Code und überträgt diesen entsprechend der für diese
    `Client_Id` registrierten `redirect_uri` an das Authenticator-Modul."

8.	"Das Anwendungssystem (PVS/KIS) verarbeitet den Authorization Code je nach
    technischer Umsetzung."

9.	"Das Anwendungssystem (PVS/KIS) übergibt den Authorization Code je nach
    technischer Umsetzung an den aufrufenden Fachdienst."

10.	"Der Authorisierungsserver des Fachdienstes erzeugt sich einen
    AES256-Token-Key, verknüpft ihn mit dem Code Verifier zum Key Verifier
    und sendet diesen unter Nutzung des öffentlichen Schlüssels `PUK_IDP_ENC`
    verschlüsselt zusammen mit dem Authorization Code zum Token-Endpunkt des
    IDP-Dienstes."

11.	"Der Token-Endpunkt entschlüsselt und validiert den Key Verifier, entnimmt
    aus diesem den Code Verifier und gleicht diesen mit der Code Challenge zu
    diesem dem Authorization Code ab.  Der Token-Endpunkt erzeugt die
    erforderlichen Token, signiert sie mit seinem privaten Schlüssel
    `PrK_IDP_SIG` und verschlüsselt sie mit dem „Token-Key“ des
    Anwendungsfrontends, welchen er dem Key Verifier entnommen hat.| Der
    Token-Endpunkt überträgt die ID-Token und Access-Token an den
    Authorisierungsserver des Fachdienstes"

12.	"Der Fachdienst entschlüsselt das ID-Token entsprechend dem gewählten
    „Token-Key“. Der Fachdienst validiert das ID-Token anhand des
    öffentlichen Schlüssels PuK-Token des Token-Endpunktes. (Optional validiert
    er zusätzlich die im Authorization Request übermittelte nonce). Fachdienste
    mit TI-Zugriff sollten zusätzlich eine OCSP-Validierung auf die Signatur
    durchführen. (Anderen Fachdiensten steht der OCSP-Responder hierfür bald
    auch im Internet zur Verfügung) Der Authorisierungsserver des Fachdienstes
    prüft die Client-Identität unter Verwendung des entschlüsselten ID-Token
    und der in Schritt 9 übertragenen Client-Attestation und erzeugt ein
    VAU-Nutzerpseudonym (VAU-NP)"

13.	"Der Authorisierungsserver des Fachdienstes gibt den Zugriff auf die
    Fachdaten frei und liefert das VAU-NP and das Aufrufer-System zurück."


## Bemerkungen

### Vorbedingung - lesen des Discovery Dokument des IDP-Dienst bei Fachdienst (AuthServer) und Authenticator (PVS)

und prüfen gegen `puk_disc_sig`

    {

       "alg": "BP256R1",
       "kid": "puk_disc_sig",
       "x5c": [
         "[Enthält das verwendete Signer-Zertifikat als Base64 ASN.1
         DER-Encoding. Hier kommt ausnahmsweise NICHT URL-safes Base64-Encoding
         zum Einsatz!]" ] 
    }

    {
       "authorization_endpoint": "[URL des Authorization Endpunkts.]",
       "federation_authorization_endpoint": "[URL des Authorization Endpunkt für Anfragen an IDPs der Föderation - dieser ist nur im Internet verfügbar]",
       "auth_pair_endpoint": "[URL des Pairing-Authorization-Endpunkts - dieser ist nur im Internet verfügbar]"      
       "third_party_authorization_endpoint" : "[URL des third_party-Authorization-Endpunkts - dieser ist nur im Internet verfügbar]"  
       "sso_endpoint": "[URL des SSO-Authorization Endpunkts.]",
       "uri_pair": "[URL des Pairing-Endpunkts.]",
       "token_endpoint": "[URL des Authorization-Endpunkts.]",
       "uri_disc": "[URL des Discovery Document.]",
       "issuer": "http://url.des.idp",
       "jwks_uri": "[URL einer JWKS-Struktur mit allen vom Server verwendeten Schlüsseln]",
       "exp": "[Gültigkeit des Token. Beispiel: 1618330390]",
       "iat": "[Zeitpunkt der Ausstellung des Token. Beispiel: 1618243990]",
       "uri_puk_idp_enc": "http://url.des.idp/idpEnc/jwk.json",
       "uri_puk_idp_sig": "http://url.des.idp/idpSig/jwk.json",
       "subject_types_supported": [
         "pairwise"
       ],

       "id_token_signing_alg_values_supported": [
         "BP256R1"
       ],

       "response_types_supported": [
         "code"
       ],

       "scopes_supported": [
         "openid",
         "e-rezept"
         "pairing"
         "ePAfa"
       ],

       "response_modes_supported": [
         "query"

       ], 

     "grant_types_supported": [
         "authorization_code"
       ],

       "acr_values_supported": [
         "gematik-ehealth-loa-high"

       ],

       "token_endpoint_auth_methods_supported": [
         "none"

       ],

       "code_challenge_methods_supported": [
         "S256"

       ]

    }



1 - siehe ePA - OAuth-Flow ePA (universell) - das PVS stellt im Rahmen des VAU
Kanal einen initialen Request an den AuthServer (OAuth?)



2 - Formulierung des `Auth_Request` vom Authorization Server an den IDP-Dienst

Aktuell unterstützt der `IDP_Dienst` weder PAR noch `mTLS_Client` Authentication für
confidential Clients. Daher erfolgt die Abfrage auf dem minimalinvasiven Weg und
nutzt den `key_verifier` mechanismus und routet den `Auth_Code` über das FdV zum
AuthServer des Fachdienstes, welcher auch die `code_challenge` bestimmt. Damit
liegen keine ID Informationen im Frontend/PVS vor.

Potentiell kann man hier den `IDP_Dienst` erweitern um analog zu den sektoralen
IDPs angesprochen zu werden (money-mouth face money-mouth face money-mouth face
money-mouth face )



3 Das Authenticator-Modul überträgt den Authorization Request weiter an den
Authorization-Endpunkt des IDP-Dienstes.

Im Gegensatz zum e-Rezept erzeugt sich das PVS keine eigene `code_challenge`
sondern leitet den Request des AuthServer weiter an den IDP-Dienst

    /sign_response?
    &client_id=eRezeptApp

Die Client-ID des Primärsystems wird beim Registrieren des Primärsystems beim
IDP festgelegt.

    &state=AcYxMQ5MZMpRh6WOBjs8

Dieser Parameter wird vom Client zufällig generiert, um CSRF zu verhindern.
Indem der Server mit diesem Wert antwortet, werden Redirects legitimiert.

    &redirect_uri="https://ePAAktensystem007.de/AS"

Die URL wird vom Primärsystem beim Registrierungsprozess im IDP hinterlegt und
leitet die Antwort des Servers an diese Adresse um.

    &code_challenge=SU8xsVcUypYGUi2g-mzs7rvR2lMtQ9vyj_9Hxs0WcII

Der Hashwert des Code-Verifier wird zum IDP als Code-Challenge gesendet. Teil
von PKCE.

    &code_challenge_method=S256

Das Primärsystem generiert einen Code-Verifier und erzeugt darüber einen Hash im
Verfahren SHA-256, hier abgekürzt als S256. Teil von PKCE.

    &response_type=code

Referenziert den erwarteten Response-Type des Flows. Muss immer 'code' lauten.
Damit wird angezeigt, dass es sich hierbei um einen Authorization Code Flow
handelt. Für eine nähere Erläuterung siehe OpenID-Spezifikation.

&nonce=nN4LkW1moAwg1tofYZtf

String zur Verhinderung von CSRF-Attacken. Dieser Wert ist optional. Wenn er
mitgegeben wird, muss der gleiche Wert im abschließend ausgegebenen `ID_TOKEN`
wiederauftauchen.

scope=openid+ePAfa

Der Scope entspricht dem zwischen ePA-Fachdienst und IDP festgelegten Wert.

    &idp_iss=kk_idp_4711

Nur für Requests an den 3rd Party `Authorization_Endpoint` des IDP-Dienstes durch
das E-Rezept-FdV. Eindeutiger interner Identifier des anzufragenden sektoralen
Identity Provider, welcher für die Authentisierung des Nutzers gewählt wurde.



4 - nichts zu beschreiben



5 - nichts zu beschreiben



6a Der Authorization-Endpunkt überträgt CHALLENGE und Consent-Abfrage
`USER_CONSENT` zum Authenticator-Modul.

    200
    Cache-Control=no-store,
    Pragma=no-cache,
    Version=0.1-SNAPSHOT,
    Content-Type=application/json,
    Transfer-Encoding=chunked,
    Date=<Zeitpunkt der Antwort. Beispiel Fri, 05 Feb 2021 12:46:20 GMT>
    Keep-Alive=timeout=60,
    Connection=keep-alive

    Response-Body:
    {
       "challenge": "eyJhbGciOiJC...",
       "user_consent": {
         "requested_scopes": {
           "ePAfa": "Zugriff auf die ePA-Funktionalität.",
           "openid": "Zugriff auf den ID_TOKEN."
         },
         "requested_claims": {
           "organizationName": "Zustimmung zur Verarbeitung der Organisationszugehörigkeit",
           "professionOID": "Zustimmung zur Verarbeitung der Rolle",
           "idNummer": "Zustimmung zur Verarbeitung der ID (z.B. Krankenversichertennummer, Telematik-ID)",
           "given_name": "Zustimmung zur Verarbeitung des Vornamens",
           "family_name": "Zustimmung zur Verarbeitung des Nachnamens",
           "display_name": "Zustimmung zur Verarbeitung des Anzeigenamens"
         }
       }
    }

    CHALLENGE_TOKEN:
    {
       "alg": "BP256R1",
       "typ": "JWT",
       "kid": "puk_idp_sig"
    }
    {
       "iss": "http://url.des.idp",
       "response_type": "code",
       "snc": "[server-nonce. Wird verwendet, um noise hinzuzufügen. Beispiel: \"yvqCVQO09TFsFfaJ312RfYoi1oII0BHtIDIRpzD8Gu0\"]",
       "code_challenge_method": "S256",
       "token_type": "challenge",
       "nonce": "nN4LkW1moAwg1tofYZtf",
       "client_id": "https://ePAAktensystem007.de/AS",
       "scope": "openid ePAfa",
       "state": "AcYxMQ5MZMpRh6WOBjs8",
       "redirect_uri": "http://redirect.gematik.de/erezept",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244172]",
       "iat": "[Zeitpunkt der Ausstellung des Token. Beispiel: 1618243992]",
       "code_challenge": "SU8xsVcUypYGUi2g-mzs7rvR2lMtQ9vyj_9Hxs0WcII",
       "jti": "[A unique identifier for the token, which can be used to prevent reuse of the token. Value is a case-sensitive string. Beispiel: \"07925bdc7c5d9990\"]"
    }



6b Das Authenticator-Modul überträgt signierte CHALLENGE mit dem
Smartcard-Zertifikat an den IDP-Dienst 

    https://<FQDN Server>/<AUTHORIZATION_ENDPOINT>
    Multiparts:
    signed_challenge=<signierter und anschließend verschlüsselter Challenge Token>

    Challenge Response (Encryption Header):
    {
       "alg": "ECDH-ES",
       "enc": "A256GCM",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244172]",
       "cty": "NJWT",
       "epk": {
         "kty": "EC",
         "x": "LgkJSQwrz1bCoFjSLhay9O7TLaQImYW7jeOF6XmpQX4",
         "y": "dTC6ri-f1QqpJp7M4LLg0lw4FzrzNc29nrrzjPwEWWc",
         "crv": "BP-256"
       }
    }
    {
       "njwt": "[Ein verschachtelt enthaltenes JWT] - Die verschlüsselte Challenge Response."
    } 



    Challenge Response (Decrypted):

    {
       "typ": "JWT",
       "cty": "NJWT",
       "alg": "BP256R1",
       "x5c": [
         "[Enthält das verwendete Signer-Zertifikat als Base64 ASN.1 DER-Encoding. Hier kommt ausnahmsweise NICHT URL-safes Base64-Encoding zum Einsatz!]"
       ]
    }

    {
       "njwt": "[Ein verschachtelt enthaltenes JWT] - Dieses Token ist die vom Server in der vorigen Nachricht übergebene Challenge. Sie muss exakt wie empfangen auch wieder übertragen werden. "
    }



7 Der Authorization-Endpunkt des IDP-Dienst überträgt den `AUTHORIZATION_CODE` und
den `SSO_TOKEN` an das Authenticator-Modul (Antwort Schritt 3).

    302
    Cache-Control=no-store,
    Pragma=no-cache,
    Location="https://ePAAktensystem007.de/AS"?
    code=<Authorization Code in Base64-URL-Safe Encoding. Wird unten detaillierter aufgeführt> Der Authorization-Code. Er berechtigt zur Abholung eines ID_TOKEN. Er ist vom IDP für den IDP verschlüsselt und dementsprechend vom Client nicht weiter zu verarbeiten.
    &state=AcYxMQ5MZMpRh6WOBjs8 Der state der Session. Sollte dem zufällig generierten state-Wert aus der initialen Anfrage entsprechen.
    &ssotoken=<SSO-Token in Base64-URL-Safe Encoding. Wird unten detaillierter aufgeführt> Der SSO-Token. Mit diesem kann der Client sich wiederholt einloggen, ohne erneut den Besitz der Karte durch Unterschreiben einer Challenge beweisen zu müssen. Er ist vom IDP für den IDP verschlüsselt und dementsprechend vom Client nicht weiter zu verarbeiten. - Nicht im Fall der Anmeldung über ein Primärsystem.   
    Content-Length=0,
    Date=<Zeitpunkt der Antwort. Beispiel Fri, 05 Feb 2021 12:46:20 GMT>
    Keep-Alive=timeout=60, Connection=keep-alive 

8 - nichts zu tun



9 Das Anwendungssystem (PVS/KIS) übergibt den Authorization Code je nach
technischer Umsetzung an den aufrufenden Fachdienst.

Der AuthServer zieht sich den verwendeten IDP und den `code_verifier` aus den
Informationen zum bestehenden VAU-Kanal mit dem PVS


10 Der AuthServer der ePA sendet `CODE_VERFIER` und `AUTHORIZATION_CODE` zum
Token-Endpunkt des IDP-Dienstes.

    https://<FQDN Server>/<TOKEN_ENDPOINT>

    Multiparts:

    client_id="https://ePAAktensystem007.de/AS"
    &code=<Authorization Code des IDP>
    &grant_type=authorization_code
    &key_verifier=<verschlüsselter KEY_VERIFIER>
    &redirect_uri="https://ePAAktensystem007.de/AS"


    Key_verifier (Encryption Header):

    {
       "alg": "ECDH-ES",
       "enc": "A256GCM",
       "cty": "JSON",
       "epk": {
         "kty": "EC",
         "x": "jqrGQlZqCGQgK7OtJj0gfWPWSbStracf_PreBrA05Lc",
         "y": "V4bbOGUgr-AV6NFLnPJNYkyPLcR_1QkGlR6w4bK1wdI",
         "crv": "BP-256"   } } 

    Key_verifier (Body)

      {
       "token_key": "T0hHOHNKOTFaREcxTmN0dVRKSURraTZxNEpheGxaUEs",
       "code_verifier": "W91A37hQ8oeDRVpnkYgpYthjl4LqYy95A87ISy9zpUM"
    }



11 Der Token-Endpunkt überträgt die Token an den AuthServer der ePA

    200
    Cache-Control=no-store,
    Pragma=no-cache,
    Version=0.1-SNAPSHOT,
    Content-Type=application/json,
    Transfer-Encoding=chunked,
    Date=<Zeitpunkt der Antwort. Beispiel Fri, 05 Feb 2021 12:46:20 GMT>
    Keep-Alive=timeout=60,
    Connection=keep-alive

    Response-Body:

    {

       "expires_in": 300,
       "token_type": "Bearer",
       "id_token": <Mit dem Token_Key verschlüsseltes ID_TOKEN.>
       "access_token": <Mit dem Token_Key verschlüsseltes ACCESS_TOKEN.>
    }

    Access Token (Encryption Header):

    {  "alg": "dir",
       "enc": "A256GCM",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244294]",
       "cty": "NJWT"
    }

    {
       "njwt": "[Ein verschachtelt enthaltenes JWT] - Das ACCESS_TOKEN"
    } 



    Access Token (Decrypted):

    {

       "alg": "BP256R1",
       "typ": "at+JWT",
       "kid": "puk_idp_sig"
    }

    {

       "sub": "[subject. Base64(sha256(audClaim + idNummerClaim + serverSubjectSalt)). Beispiel: \"ez4D403gBzH1IhnYOXA4aUU-7spqPbWUyUELPoA79CM\"]",
       "professionOID": " 1.2.276.0.76.4.51",
       "idNummer": "X-X.XX.X.X.XXXX",
       "organizationName": "Zahnarztpraxis Dr. Tiefbohrer",
       "amr": [
         "mfa",
         "sc",
         "pin"   ],

       "iss": "http://url.des.idp",
       "given_name": "Juna",
       "display_name": "",
       "client_id": "https://ePAAktensystem007.de/AS",
       "acr": "gematik-ehealth-loa-high",
       "aud": "https://ePAAktensystem007.de/AS",
       "azp": ""https://ePAAktensystem007.de/AS"",
       "scope": "openid ePAfa",
       "auth_time": "[Timestamp der Authentisierung. Beispiel: 1618243993]",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244294]",
       "family_name": "Fuchs",
       "iat": "[Zeitpunkt der Ausstellung des Token. Beispiel: 1618243994]",
       "jti": "[A unique identifier for the token, which can be used to prevent reuse of the token. Value is a case-sensitive string. Beispiel: \"c0bf3cebe428e3c9\"]"
    }



    ID Token (Encryption Header):

    {

       "alg": "dir",
       "enc": "A256GCM",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244294]",
       "cty": "NJWT"
    }

    {
       "njwt": "[Ein verschachtelt enthaltenes JWT] - Das ID Token"
    }



    ID Token (Decrypted):

    {
       "alg": "BP256R1",
       "typ": "JWT",
       "kid": "puk_idp_sig"
    }

    {
       "at_hash": "[Erste 16 Bytes des Hash des Authentication Token Base64(subarray(Sha256(authentication_token), 0, 16)). Beispiel: \"5AZmDxrYImUa6-kjMNAL3g\"]",
       "sub": "[subject. Base64(sha256(audClaim + idNummerClaim + serverSubjectSalt)). Beispiel: \"ez4D403gBzH1IhnYOXA4aUU-7spqPbWUyUELPoA79CM\"]",
       "organizationName": "Zahnarztpraxis Dr. Tiefbohrer",
       "professionOID": " 1.2.276.0.76.4.51",
       "idNummer": "X-X.XX.X.X.XXXX",
       "amr": [
         "mfa",
         "sc",
         "pin"   ],
       "iss": "http://url.des.idp",
       "given_name": "Juna",
       "display_name": "",
       "aud": "https://ePAAktensystem007.de/AS",
       "acr": "gematik-ehealth-loa-high",
       "azp": "https://ePAAktensystem007.de/AS",
       "auth_time": "[Timestamp der Authentisierung. Beispiel: 1618243993]",
       ""scope": "openid ePAfa",
       "exp": "[Gültigkeit des Token. Beispiel: 1618244294]",
       "iat": "[Zeitpunkt der Ausstellung des Token. Beispiel: 1618243994]",
       "family_name": "Fuchs",
       "jti": "[A unique identifier for the token, which can be used to prevent reuse of the token. Value is a case-sensitive string. Beispiel: \"c1c760ca67fe1306\"]"
    }



12+ die ePA-VAU prüft das ID-Token

