#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import cbor2, hashlib, os, requests, sys

# für's debugging
from icecream import ic

sys.path.append(os.path.join(os.path.dirname(__file__), '../minimal/'))

import encvau, kemvau


class VAUKanal():
    """
    xxx
    """
    def __init__(self, AS_URL: str):
        """
        xxx
        """
        assert AS_URL
        assert AS_URL.startswith("http://") or AS_URL.startswith("https://")
        assert AS_URL.endswith("/")

        self.AS_URL = AS_URL
        AS_URL_START_VAU = AS_URL + "VAU"

        self.https_session = requests.Session()

        client_schluessel_1 = kemvau.gen_keypairs()

        nachricht_1 = {
                "MessageType" : "M1",
                "ECDH_PK"     : client_schluessel_1["ECDH"]["pub_key"],
                "Kyber768_PK" : client_schluessel_1["Kyber768"]["pub_key"]
                }

        nachricht_1_encoded = cbor2.dumps(nachricht_1)
        transscript_client = nachricht_1_encoded

        # hier kommt je nach Verfügbarkeit auch in 'headers'
        # das VAU-NP
        http_response = self.https_session.post(
                     AS_URL_START_VAU,
                     headers={'Content-Type': 'application/cbor'},
                     data=nachricht_1_encoded,
                     timeout=5
                 )

        assert http_response.status_code == requests.codes.ok
        assert http_response.headers['Content-Type'] == "application/cbor"
        assert 'VAU-CID' in http_response.headers
        vau_cid = http_response.headers['VAU-CID']
        self.AS_URL_plus_VAU_CID = self.AS_URL + vau_cid

        nachricht_2_encoded = http_response.content
        # xxx, len>0 ?
        transscript_client += nachricht_2_encoded

        # xxx, muss im Produktiv-Code in eine try-Umgebung
        nachricht_2 = cbor2.loads(nachricht_2_encoded)

        #
        # Client: nachricht-3 VAUClientKEM+KeyConfirmation
        #
        client_kem_result_1 = kemvau.decapsulation(nachricht_2, client_schluessel_1)
        ic("Schlüsselableitung für die K1-Schlüssel")
        (C_K1_c2s, C_K1_s2c) = kemvau.kem_kdf(client_kem_result_1)
        transfered_signed_vau_server_pub_keys = kemvau.AEAD_dec(
                C_K1_s2c, nachricht_2["AEAD_ct"])
        # Im Produktivcode try-Umgebung, weil daten noch nicht authentisiert
        signed_vau_server_pub_keys = cbor2.loads(transfered_signed_vau_server_pub_keys)

        # Zertifikate signed_vau_server_pub_keys["cert"] prüfen
        #xxx

        # Signatur signed_vau_server_pub_keys["signature-ES256"] prüfen
        #xxx

        pub_keys = cbor2.loads(signed_vau_server_pub_keys["signed_pub_keys"])
        # prüfen von pub_key["exp"]
        #xxx

        client_kem_result_2 = kemvau.encapsulation(pub_keys)
        # ERP = Enforce Replay Protection
        # ESO = Enforce Sequence Order
        nachricht_3_inner_layer = {
                "ECDH_ct"    : client_kem_result_2["ECDH_ct"],
                "Kyber768_ct" : client_kem_result_2["Kyber768_ct"],
                "ERP" : False,
                "ESO" : False
        }
        nachricht_3_inner_layer_encoded = cbor2.dumps(nachricht_3_inner_layer)
        aead_ciphertext_msg_3 = kemvau.AEAD_enc(C_K1_c2s, nachricht_3_inner_layer_encoded)
        transscript_client_to_send = transscript_client + aead_ciphertext_msg_3

        ic("Schlüsselableitung für die K2-Schlüssel")
        (C_K2_c2s_KeyConfirmation, C_K2_c2s_AppData, C_K2_s2c_KeyConfirmation,
         C_K2_s2c_AppData, C_KeyID) = kemvau.kem_kdf(client_kem_result_1, client_kem_result_2)
        transscript_client_hash = hashlib.sha256(transscript_client_to_send).digest()
        aead_ciphertext_msg_3_key_confirmation = \
            kemvau.AEAD_enc(C_K2_c2s_KeyConfirmation, transscript_client_hash)

        nachricht_3 = {
                "MessageType" : "M3",
                "AEAD_ct"     : aead_ciphertext_msg_3,
                "AEAD_ct_key_confirmation" : aead_ciphertext_msg_3_key_confirmation
                }

        nachricht_3_encoded = cbor2.dumps(nachricht_3)
        transscript_client += nachricht_3_encoded

        http_response = self.https_session.post(
                     self.AS_URL_plus_VAU_CID,
                     headers={'Content-Type': 'application/cbor'},
                     data=nachricht_3_encoded,
                     timeout=5
                 )

        ic(http_response.status_code)
        # ic(http_response.content)
        assert http_response.status_code == requests.codes.ok
        assert http_response.headers['Content-Type'] == "application/cbor"

        nachricht_4_encoded = http_response.content

        # xxx 
        # Key-Confirmation mit M4 machen / Transscripts überprüfen




if __name__ == '__main__':

    sys.exit("I'm not to be called directly.")

