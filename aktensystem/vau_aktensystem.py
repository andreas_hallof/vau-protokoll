#! /usr/bin/python

import cbor2, zmq
import VAUProtokollBackend

# für's debugging
from icecream import ic


zmq_context = zmq.Context()
vau_socket = zmq_context.socket(zmq.REP)
vau_socket.bind("tcp://127.0.0.1:5555")

VAU_Kanäle = {}

print("OK ready, starting event-loop, listening ...")

while True:
    #  Wait for next request from client
    message_encoded = vau_socket.recv()
    print("Got req, size=", len(message_encoded))

    try:
        # sanity-checks für "message"
        #xxx
        message = cbor2.loads(message_encoded)
    except ValueError as e:
        print("hm", e)
        vau_socket.send(cbor2.dumps({"Status": "Error", "ErrorData": str(e)}))
        continue

    if message["MessageType"] == "M1":
        print("Size client message:", len(message["Data"]))
        neuer_vau_kanal = VAUProtokollBackend.VAUKanalBackend(message["Data"])
        VAU_Kanäle[neuer_vau_kanal.vau_cid] = neuer_vau_kanal
        res = cbor2.dumps({"Status": "OK",
                           "Data": neuer_vau_kanal.nachricht_2_encoded,
                           "VAU-CID": neuer_vau_kanal.vau_cid})
        print("M1 OK")
    elif message["MessageType"] == "M3":
        if not message["VAU-CID"] in VAU_Kanäle:
            vau_socket.send(cbor2.dumps({"Status": "Error",
                                         "ErrorCode": 9,
                                         "ErrorMessage": "unknown CID"}))
            continue
        my_vau_kanal = VAU_Kanäle[message["VAU-CID"]]
        my_vau_kanal.finalize(message["Data"])

    elif message["MessageType"] == "Octet-Stream":
        pass
    else:
        pass

    #  Send reply back to client
    print("Sending", len(res), "bytes as response.")
    vau_socket.send(res)

