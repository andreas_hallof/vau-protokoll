#! /usr/bin/env bash

# Komponenten-PKI

openssl ecparam -name prime256v1 -genkey -out komponenten-pki-ca-key.pem

openssl req -x509 -key komponenten-pki-ca-key.pem \
    -outform pem \
    -out komponenten_pki_ca.pem -days 365 \
    -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=Komponenten-PKI-CA"

openssl ecparam -name prime256v1 -genkey -out vau-sig-key.pem

openssl req -new -key vau_sig_key.pem \
        -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=VAU-Aktensystem" \
        -out vau_sig_cert.csr -sha256

openssl x509 -req -in vau_sig_cert.csr -CA komponenten-pki-ca.pem -CAkey komponenten-pki-ca-key.pem \
        -CAcreateserial -outform DER -out vau_sig_cert.der -days 365 -sha256

