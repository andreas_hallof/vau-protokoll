#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx
"""

from flask import Flask, Response, request, redirect
from starlette import status
import cbor2, hashlib, redis, requests, time, zmq

# fürs debugging
from icecream import ic

app = Flask(__name__)
redis_con = redis.Redis(host='localhost', port=6379)
req_session = requests.Session()

zmq_context = zmq.Context()
vau_socket = zmq_context.socket(zmq.REQ)
vau_socket.connect("tcp://127.0.0.1:5555")

DEBUGGING = False

@app.route("/", methods=['GET'])
def default_route_get():
    """
        Ein ePA-Client fängt für den Handshake bei Pfadname "/VAU" an
        und nicht bei "/".
    """
    return """Der VAU-Protokoll-Handshake beginnt mit einem POST auf /VAU."""

@app.route("/", methods=['POST'])
def default_route_post():
    """
        Ein ePA-Client fängt für den Handshake bei Pfadname "/VAU" an
        und nicht bei "/".
    """
    return redirect("/VAU", code=302)

@app.route("/VAU", methods=['POST'])
def vau_start_route():
    """
    Hier kommt ein initialer VAU-Protokoll-Request
    bei mir an.
    """
    if request.content_type != "application/cbor":
        return "Wrong Content-Type", status.HTTP_400_BAD_REQUEST
    req_data = request.get_data()

    # Bevor ich den Request an eine VAU-Instanz weiterleite 
    # kurz ein paar sanity-checks machen
    try:
        if len(req_data) != 1306:
            ic(len(req_data))
            return "M1 has wrong length", 400
        nachricht_1_decoded = cbor2.loads(req_data)
        if nachricht_1_decoded["MessageType"] != "M1":
            return "has to be a message M1", 400
    except ValueError as e:
        print("hm", e)
        # xxx
        return "xxx", 400

    # VAU-NP holen
    # xxx

    # xxx Routing-Entscheidung treffen, auf welche VAU-Instanz der Request
    # kommen soll.
    # xxx ich habe der Einfachheit halber mal hier im Beispiel nur genau 
    # eine VAU-Instanz

    vau_socket.send(cbor2.dumps({"MessageType": "M1", "Data" : req_data}))
    vau_response = vau_socket.recv()

    vau_cid = None
    try:
        assert len(vau_response)>0
        vau_response = cbor2.loads(vau_response)
        if vau_response["Status"] == "OK":
            res = vau_response["Data"]
            assert "VAU-CID" in vau_response
            vau_cid = vau_response["VAU-CID"]
        else:
            assert "ErrorData" in vau_response
            res = cbor2.dumps({"MessageType": "Error", "ErrorCode": 1,
                                "ErrorMessage": vau_response["ErrorData"]})
    except ValueError as e:
        res = cbor2.dumps({"MessageType": "Error", "ErrorCode": 1,
                           "ErrorMessage": "hm " + str(e)})


    resp = Response(res, mimetype="application/cbor")
    if vau_cid:
        resp.headers["VAU-CID"] = vau_cid
    return resp

@app.route("/VAU-mit-CID/<last_part_of_cid>", methods=['POST'])
def vau_mit_cid_route(last_part_of_cid: str):
    """
    hier kommen
        1) Nachicht M3 oder
        2) verschlüsselte Requests an eine VAU-Instanz nach erfolgreichen
           VAU-Protoll-Handshake an.
    """

    # merker zugriff auf last_part_of_cid auch über 
    # view_args["last_part_of_cid"] möglich
    # assert section == request.view_args['section']

    cid = "/VAU-mit-CID/" + last_part_of_cid
    # Prüfen ob es CID überhaupt gibt.
    # Nach spätestens 24 Stunden muss ein AS/VAU die Verbindung löschen 
    # -> CID kann evtl. gar nicht mehr existieren

    #xxx, Fehler zurückmelden, falls CID nicht mehr existiert 

    # CID gibt es also, los geht es
    req_data = request.get_data()

    if request.content_type == "application/cbor":
        # Bevor ich den Request an eine VAU-Instanz weiterleite 
        # kurz ein paar sanity-checks machen
        try:
            nachricht_3_decoded = cbor2.loads(req_data)
            if nachricht_3_decoded["MessageType"] != "M3":
                return "has to be a message M3", 400
        except ValueError as e:
            print("hm", e)
            # xxx
            return "xxx", 400

        vau_socket.send(cbor2.dumps({"MessageType": "M3",
                                     "VAU-CID": cid,
                                     "Data" : req_data}))
        vau_response = vau_socket.recv()

        try:
            assert len(vau_response)>0
            vau_response = cbor2.loads(vau_response)
            print(vau_response["Status"]);
            # ic(vau_response)
            if vau_response["Status"] == "OK":
                res = vau_response["Data"]
            else:
                assert "ErrorMessage" in vau_response
                res = cbor2.dumps({"MessageType": "Error", "ErrorCode": 1,
                                   "ErrorMessage": vau_response["ErrorMessage"]})
        except ValueError as e:
            res = cbor2.dumps({"MessageType": "Error", "ErrorCode": 1,
                               "ErrorMessage": "hm " + str(e)})

        return Response(res, mimetype="application/cbor")


    if request.content_type != "application/octet-stream":
        return "Wrong Content-Type", status.HTTP_400_BAD_REQUEST


    pass

if __name__ == '__main__':
    app.run(port=8081, debug=True)

