#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Die beiden VAUServer-Schlüsselpaare erzeugen. Diese werden von einer VAU
erzeugt und dann über die AUT-Identität der VAU (deren privater Schlüssel sich
in dem VAU-HSM befindet) signiert.

"""

from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

from base64 import b64encode
from binascii import hexlify
from icecream import ic
import cbor2, kemvau, time

# Los geht's

if __name__ == '__main__':

    ic.configureOutput(argToStringFunction=kemvau.toString)
    vau_server_schlüssel = kemvau.gen_keypairs()

    # Diese beiden Schlüsselpaar werden in einer VAU-Instanz erzeugt
    # und die öffentlichen Schlüssel über die im HSM befindliche 
    # AUT-Identität signiert. Das Ergebnis ist dann 
    # "signierte öffentliche VAU-Schlüssel" (A_24425)
    vau_server_keys = {
            "ECDH_PK"       : vau_server_schlüssel["ECDH"]["pub_key"],
            "ECDH_PrivKey"  : \
                    # Die genau Wahl der Kodierung hier irrelevant,
                    # weil nur VAU-intern verwendet.
                vau_server_schlüssel["ECDH"]["priv_key"].private_bytes(
                         encoding=serialization.Encoding.DER,
                         format=serialization.PrivateFormat.PKCS8,
                         encryption_algorithm=serialization.NoEncryption()),
            "Kyber768_PK"    : vau_server_schlüssel["Kyber768"]["pub_key"],
            "Kyber768_PrivKey" : vau_server_schlüssel["Kyber768"]["priv_key"]
            }

    #ic(vau_server_keys)

    with open("vau_server_keys.cbor", "wb") as server_keys_file:
        server_keys_file.write(cbor2.dumps(vau_server_keys))

    vau_server_pub_keys = {
            "ECDH_PK"       : vau_server_keys["ECDH_PK"],
            "Kyber768_PK"    : vau_server_keys["Kyber768_PK"],
            "iat" : int(time.time()),
            "exp" : int(time.time())+24*60*60*7,
            "comment" : "Erzeugt bei VAU-Instanz xyz, Meta-Info abcd"
            }
    ic("VAU_Keys (A_24425): ", vau_server_pub_keys)

    vpks_encoded = cbor2.dumps(vau_server_pub_keys)

    with open("../aktensystem/pki/vau-sig-key.pem", "rb") as priv_key_file:
        vau_sign_priv_key = serialization.load_pem_private_key(priv_key_file.read(), password=None)
    signature = vau_sign_priv_key.sign(vpks_encoded, ec.ECDSA(hashes.SHA256()))

    with open("../aktensystem/pki/vau_sig_cert.der", "rb") as aut_cert_file:
        vau_aut_cert = aut_cert_file.read()

    with open("../aktensystem/pki/ocsp-response-vau-sig.der", "rb") as vau_aut_cert_ocsp_file:
        vau_aut_cert_ocsp_response = vau_aut_cert_ocsp_file.read()

    result = {"signed_pub_keys" : vpks_encoded,
              "signature-ES256" : signature,
              "cert"            : vau_aut_cert,
              "ocsp_response"   : vau_aut_cert_ocsp_response}
    ic("signierte öffentliche VAU-Schlüssel (A_24425) kurz vor der finalen CBOR-Kodierung",
       result)
    with open("vau_server_signed_pub_keys.cbor", "wb") as sspks:
        sspks.write(cbor2.dumps(result))

