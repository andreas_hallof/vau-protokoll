#! /bin/bash

tmux new-session -d -s services -n vau-as ./aktensystem/vau_aktensystem.py
tmux new-session -d -s services -n as ./aktensystem/aktensystem.py
tmux new-window -t services -n bash /bin/bash

cat <<'EOF'

... services werden gestartet.

Je nach Umgebung (Rechner) muss man dem HTTP-Server zwei bis fünf
Sekunden zum Start-Up Zeit lassen, bis man mit 
    ./oqs-python/client.py
die Anfrage starten.

mit 
    tmux at -t services 
kann man -- wenn man möchte -- sich zur Session verbinden.

EOF

