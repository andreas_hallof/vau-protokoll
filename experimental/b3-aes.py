#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3

# debugging infos
from icecream import ic
import debugging

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    mykey  = b' '*32
    header = b"Test"
    plaintext = b"Hallo Plaintext"

    my_cipher = botan3.SymmetricCipher("AES-256/GCM", encrypt=True)
    my_cipher.set_key(mykey)
    my_cipher.set_assoc_data(header)
    iv = b' '*12
    my_cipher.start(iv)
    ciphertext = my_cipher.finish(plaintext)

    ic(iv)
    ic(ciphertext)

    my_cipher = botan3.SymmetricCipher("AES-256/GCM", encrypt=False)
    my_cipher.set_key(mykey)
    my_cipher.set_assoc_data(header)
    iv = b' '*12
    my_cipher.start(iv)
    my_plaintext = my_cipher.finish(ciphertext)

    ic(my_plaintext)
    ic(my_plaintext.decode())

