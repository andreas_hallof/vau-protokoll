#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3

# Interoperabilität zu openssl/python-cryptography testen
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF

import sys
sys.path.append("..")

import debugging
from icecream import ic

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    data = b" "*32

    r1 = HKDF(algorithm=hashes.SHA256(), length=32, salt=None,
               info=b'').derive(data)
    ic(r1)

    r2 = botan3.kdf("HKDF(SHA-256)", data, 32, b"", b"")

    ic(r2)

    r3 = HKDF(algorithm=hashes.SHA256(), length=32, salt=b'',
               info=b'').derive(data)
    ic(r3)

