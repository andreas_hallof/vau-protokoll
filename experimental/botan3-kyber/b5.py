#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3 as b

import oqs
import oqs.rand as oqsrand

import sys
sys.path.append("..")

import debugging
from icecream import ic
from binascii import hexlify, unhexlify

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    r = b.RandomNumberGenerator(rng_type='system')

    priv = b.PrivateKey.create("Kyber", "Kyber-768-r3", r)
    pub  = priv.get_public_key()
    pub_encoded = pub.to_der()

    #ic(pub_encoded)
    #with open("pk2.der", "wb") as f:
    #    f.write(pub2_encoded)

    K_ASN1_PREFIX = unhexlify("308204b4300d060b2b0601040181c52a010702038204a100")
    L = len(K_ASN1_PREFIX)
    #print(L)
    raw_public_key = pub_encoded[L:]
    #ic(P2)


    with oqs.KeyEncapsulation("Kyber768") as server:
        Kyber768_ct, Kyber768_shared_secret = server.encap_secret(\
                        raw_public_key)
    ic("OQS Kyber768 (also R3/3.02)", Kyber768_shared_secret)

    dec = b.KemDecrypt(priv, "Raw")
    test_b = dec.decrypt_shared_key(salt=b'', desired_key_len=32, encapsulated_key=Kyber768_ct)
    ic("botan3 Decapsulation", test_b)

