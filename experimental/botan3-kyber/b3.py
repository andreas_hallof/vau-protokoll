#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3 as b

import sys
sys.path.append("..")

import debugging
from icecream import ic
from binascii import hexlify, unhexlify

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    r = b.RandomNumberGenerator(rng_type='system')
    priv1 = b.PrivateKey.create("ecdh", "secp256r1", r)
    pub1  = priv1.get_public_key()
    export1 = pub1.to_der()
    ic(export1)
    print(hexlify(export1))
    ASN1_PREFIX = unhexlify("3057301106052b8104010c06082a8648ce3d030107034200")
    assert len(export1) == len(ASN1_PREFIX) + 1 + 32 + 32
    assert export1.startswith(ASN1_PREFIX)
    # Prefix und '\x04' wird abgeschnitten
    tmp = export1[len(ASN1_PREFIX)+1:]
    assert len(tmp) == 64
    x = tmp[:32]
    assert len(x) == 32
    y = tmp[32:]
    assert len(y) == 32

    priv2 = b.PrivateKey.create("Kyber", "Kyber-768-r3", r)
    pub2  = priv2.get_public_key()
    pub2_encoded = pub2.to_der()

    ic(pub2_encoded)
    with open("pk2.der", "wb") as f:
        f.write(pub2_encoded)

    K_ASN1_PREFIX = unhexlify("308204b4300d060b2b0601040181c52a010702038204a100")
    L = len(K_ASN1_PREFIX)
    print(L)
    P2 = pub2_encoded[L:]
    ic(P2)

    sys.exit(0)

    k_kem = b.KemEncrypt(pub2, "HKDF(SHA-256)")
    (k_ss, k_ct) = k_kem.create_shared_key(r, b"", 32)

    ic(k_ss)
    ic(k_ct)

