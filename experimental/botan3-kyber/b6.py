#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3 as b

import oqs
import oqs.rand as oqsrand

import sys
sys.path.append("..")

import debugging
from icecream import ic
from binascii import hexlify, unhexlify

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    random_source = b.RandomNumberGenerator(rng_type='system')

    priv = b.PrivateKey.create("Kyber", "Kyber-768-r3", random_source)
    pub  = priv.get_public_key()
    ic(pub)
    pub_encoded = pub.to_der()

    #ic(pub_encoded)
    #with open("pk2.der", "wb") as f:
    #    f.write(pub2_encoded)

    K_ASN1_PREFIX = unhexlify("308204b4300d060b2b0601040181c52a010702038204a100")
    L = len(K_ASN1_PREFIX)
    #print(L)
    raw_public_key = pub_encoded[L:]
    #ic(P2)


    #k_kem = b.KemEncrypt(pub2, "HKDF(SHA-256)")
    #(k_ss, k_ct) = k_kem.create_shared_key(r, b"", 32)

    with oqs.KeyEncapsulation("Kyber768") as server:
        Kyber768_ct, Kyber768_shared_secret = server.encap_secret(\
                        raw_public_key)
    ic("OQS Kyber768 (also R3/3.02)", Kyber768_shared_secret)

    dec = b.KemDecrypt(priv, "Raw")
    test_b = dec.decrypt_shared_key(salt=b'', desired_key_len=32, encapsulated_key=Kyber768_ct)
    ic("botan3 Decapsulation", test_b)

    enc = b.KemEncrypt(pub, "Raw")
    (shared_secret_3, encapsulation_key) = enc.create_shared_key(random_source,
                                                                 b"", 32)
    ic(shared_secret_3)
    ic(encapsulation_key)
    dec = b.KemDecrypt(priv, "Raw")
    test_c = dec.decrypt_shared_key(salt=b'', desired_key_len=32, encapsulated_key=Kyber768_ct)
    ic("botan3 Decapsulation", test_c)

