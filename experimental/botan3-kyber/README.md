# botan3-kyber768 vs oqs-kyber768

# mit "Raw"
Man muss "Raw" als KDF-Identifier nehmen:

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 
        23db67b0e92e1b9e135399db00fc91651a1515afe2cc04f30b41d32cedbbff8e
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump) 
        23db67b0e92e1b9e135399db00fc91651a1515afe2cc04f30b41d32cedbbff8e

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 
        dd0c32926d4f96990ce41c3fe9517cedd3e99c58adebbed369ca7763be903e5a
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump) 
        dd0c32926d4f96990ce41c3fe9517cedd3e99c58adebbed369ca7763be903e5a

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        1e2d52f4215bd1626a480ae7ee27f6d5bd5ab50291a8f52cb54e2636f8c3b1c1
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        1e2d52f4215bd1626a480ae7ee27f6d5bd5ab50291a8f52cb54e2636f8c3b1c1

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        b5f128220a1609c55d07854b6043e06e0985cbf0a14e0cb9b4979aa3547ec1b1
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        b5f128220a1609c55d07854b6043e06e0985cbf0a14e0cb9b4979aa3547ec1b1

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        95a26c0bc85ef35bcc80a3f0fad0c240d97279e6ad9681405aed5928672ec8bf
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        95a26c0bc85ef35bcc80a3f0fad0c240d97279e6ad9681405aed5928672ec8bf

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        e08b595b4b1b787c351274b747da02c55785e8617758bdfe439c3f638396322f
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        e08b595b4b1b787c351274b747da02c55785e8617758bdfe439c3f638396322f

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        855bc7bf263646aca98202a46b61a9f14b20d6b4a5b40d7933eeddac0cabc9c4
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        855bc7bf263646aca98202a46b61a9f14b20d6b4a5b40d7933eeddac0cabc9c4

    a@t15:~/git/vau-protokoll/experimental$ ./b5.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump)
        458a1727fdfdcc2430c3ac13d851a0e3da8ac2f4b65bdcd01080552ec1dd69ea
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_b: (hexdump)
        458a1727fdfdcc2430c3ac13d851a0e3da8ac2f4b65bdcd01080552ec1dd69ea

# ohne "Raw"
Schlussfolgerung botan3 und oqs verwenden die gleiche Kyber768 Version (R3 = 3.02),
aber botan3 macht mit dem erhaltenden Kyber768-Shared-Secret hoch eine 
HKDF(SHA-256) hinterher.

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 0d329ef8fe9488d346491842961eb675f6edebcaeb5f712bc9655d7f872863f1
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) 92748d312070b8146d586c0feccc1c28748814b4791cbd2772172361b59b9382
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) 92748d312070b8146d586c0feccc1c28748814b4791cbd2772172361b59b9382

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 4de13d669908813dfd9107f745f34f5af792096ee8c7ff155deaf89db0c9f816
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) ebc8671c3eeef61f7fcc5085b4cf5dd57d7ee616601d41699b3fc710a7ae9cd1
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) ebc8671c3eeef61f7fcc5085b4cf5dd57d7ee616601d41699b3fc710a7ae9cd1

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 57c25b773828505b1fbb03bbc96e9435cfdca2bac9c9bbeb7cd3355a3677b670
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) f9bec8a44d1db99fdcf07babc0c2cbf1c663ca002d47d0829e123f187361edb9
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) f9bec8a44d1db99fdcf07babc0c2cbf1c663ca002d47d0829e123f187361edb9

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 2c9c34510b898a2739c0f480e37680ef16f46df8eabeb3fe358bd4a25f9243e0
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) 7c3165e0894fe5013cd9807c8c5bdbcd57dbae02bf86308bf180d965b8e74c31
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) 7c3165e0894fe5013cd9807c8c5bdbcd57dbae02bf86308bf180d965b8e74c31

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) 2cf38fc4dd5789c90d0f319bffb1b930b01e45828a27bed410ddc3661ee74878
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) f16eb1e9199a3687e0f0e18ec8bbe257551080ec0efbe919953bd429159a5eb5
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) f16eb1e9199a3687e0f0e18ec8bbe257551080ec0efbe919953bd429159a5eb5

    a@t15:~/git/vau-protokoll/experimental$ ./b4.py 
    ic| "OQS Kyber768 (also R3/3.02)": 'OQS Kyber768 (also R3/3.02)'
        Kyber768_shared_secret: (hexdump) e1f4a6e54c03e19cb61724743b53b4f16a35b7dab8d0b1cdfd427c8c89774942
    ic| "Ich machen von Hand nochmal eine HKDF(SHA-256)": 'Ich machen von Hand nochmal eine HKDF(SHA-256)'
        test_1: (hexdump) 5d423fd8e052abc5127b6fa8f3715e00db8bd44d81bd948afdfc7d13dc311e9c
    ic| "botan3 Decapsulation": 'botan3 Decapsulation'
        test_2: (hexdump) 5d423fd8e052abc5127b6fa8f3715e00db8bd44d81bd948afdfc7d13dc311e9c
