#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Experiementieren mit Botan3
"""

import botan3

# Interoperabilität zu openssl/python-cryptography testen
from cryptography.hazmat.primitives.asymmetric import ec

import sys
sys.path.append("..")

import debugging
from icecream import ic
from binascii import hexlify, unhexlify

if __name__ == '__main__':
    ic.configureOutput(argToStringFunction=debugging.toString)

    random_source = botan3.RandomNumberGenerator(rng_type='system')
    ecdh_prk = botan3.PrivateKey.create("ecdh", "secp256r1", random_source)
    ecdh_pk = ecdh_prk.get_public_key()

    ic(ecdh_pk.get_public_point())

    ecdh_pk_encoded = ecdh_pk.to_der()

    ic(ecdh_pk_encoded)

    with open("pk1.der", "wb") as f:
        f.write(ecdh_pk_encoded)

    ASN1_PREFIX = unhexlify("3057301106052b8104010c06082a8648ce3d030107034200")

    assert len(ecdh_pk_encoded) == len(ASN1_PREFIX) + 1 + 32 + 32
    assert ecdh_pk_encoded.startswith(ASN1_PREFIX + b'\x04')
    # Prefix und '\x04' wird abgeschnitten
    tmp = ecdh_pk_encoded[len(ASN1_PREFIX)+1:]; assert len(tmp) == 64
    x = tmp[:32]; assert len(x) == 32
    y = tmp[32:]; assert len(y) == 32



    k2_prk = botan3.PrivateKey.create("ecdh", "secp256r1", random_source)
    #k2_pk = ecdh_prk.get_public_key()

    k_a_1 = botan3.PKKeyAgreement(k2_prk, "Raw")
    #ic(k2_pk.to_der())
    #ic(k_a_1.public_value())
    o_1 = k_a_1.public_value()
    ic(o_1)

    k_a_0 = botan3.PKKeyAgreement(ecdh_prk, "Raw")
    o_0 = k_a_0.public_value()
    ic(o_0)
    ic(ecdh_pk_encoded[len(ASN1_PREFIX):])

    ss_1 = k_a_1.agree(o_0, 32, b"")
    ic(ss_1)
    ss_0 = k_a_0.agree(o_1, 32, b"")
    ic(ss_1)

    ic("jetzt IOP-Test")

    e3_prk = ec.generate_private_key(ec.SECP256R1())
    e3_pub = e3_prk.public_key()
    e3_pn = e3_pub.public_numbers()
    e3_pub_encoded = b'\x04' + e3_pn.x.to_bytes(length=32, byteorder='big', \
                                               signed=False) + \
                              e3_pn.y.to_bytes(length=32, byteorder='big', \
                                               signed=False)

    tmp_x = o_0[1:33]; assert len(tmp_x) == 32
    tmp_y = o_0[1+32:]; assert len(tmp_y) == 32

    remote_pn = ec.EllipticCurvePublicNumbers(
            int.from_bytes(tmp_x, byteorder='big', signed=False),
            int.from_bytes(tmp_y, byteorder='big', signed=False),
            ec.SECP256R1())
    remote_pub = remote_pn.public_key()

    e3_shared_secret = e3_prk.exchange(ec.ECDH(), remote_pub)
    ic(e3_shared_secret)

    ss_fuer_e3 = k_a_0.agree(e3_pub_encoded, 32, b"")
    ic(ss_fuer_e3)

