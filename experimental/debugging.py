#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Debugging Zeug
"""

# debugging infos
from icecream import ic
from binascii import hexlify

# Debugging-Ausgabe hübscher machen:
def toString(obj):
    if isinstance(obj, dict):
        new_dict = dict()
        for i in obj:
            if isinstance(obj[i], bytes) or isinstance(obj, bytearray):
                new_dict[i] = "(hexdump L={}) ".format(len(obj[i])) \
                                + hexlify(obj[i]).decode()
            elif isinstance(obj[i], dict):
                tmp = dict()
                for my_i in obj[i]:
                    if isinstance(obj[i][my_i], bytes) or isinstance(obj, bytearray):
                        tmp[my_i] = "(hexdump L={}) ".format(len(obj[i][my_i]))\
                                + hexlify(obj[i][my_i]).decode()
                    else:
                        tmp[my_i] = obj[i][my_i]
                new_dict[i]=tmp
            else:
                new_dict[i] = obj[i]
        return json.dumps(new_dict, indent=4)

    if isinstance(obj, bytes):
        return "(hexdump L={}) ".format(len(obj)) + hexlify(obj).decode()

    return repr(obj)

if __name__ == '__main__':
    print("Don't call me, only import me")

