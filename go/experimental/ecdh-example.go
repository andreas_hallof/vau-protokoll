package main

import (
    "bytes"
    "crypto/ecdh"
    "crypto/rand"
    "log"
)

func main() {

    clientCurve := ecdh.P256()
    clientPrivKey, err := clientCurve.GenerateKey(rand.Reader)
    if err != nil {
        log.Fatalf("Error: %v", err)
    }
    clientPubKey := clientPrivKey.PublicKey()

    serverCurve := ecdh.P256()
    serverPrivKey, err := serverCurve.GenerateKey(rand.Reader)
    if err != nil {
        log.Fatalf("Error: %v", err)
    }
    serverPubKey := serverPrivKey.PublicKey()

    clientSecret, err := clientPrivKey.ECDH(serverPubKey)
    if err != nil {
        log.Fatalf("Error: %v", err)
    }
    serverSecret, err := serverPrivKey.ECDH(clientPubKey)
    if err != nil {
        log.Fatalf("Error: %v", err)
    }
    if !bytes.Equal(clientSecret, serverSecret) {
        log.Fatalf("The secrets do not match")
    }
    log.Printf("The secrets match")
}

