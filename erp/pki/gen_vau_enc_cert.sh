#! /usr/bin/env bash

openssl ecparam -name brainpoolP256r1 -genkey -out vau_key.pem
#openssl ecparam -name prime256v1 -genkey -out key_$i.pem

openssl req -x509 -key vau_key.pem \
-out vau_cert.pem -days 365 \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=E-Rezept-VAU Beispielimplementierung"

