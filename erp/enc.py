#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import secrets, logging
from binascii import hexlify

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%dT%H:%M:%S%z")

with open("pki/vau_cert.der", "rb") as f:
    # Das Zertifikat für die E-Rezept-VAU-Identität kann mann 
    # über /VAUCertificate (vgl. A_20160) erhalten oder über
    # /CertList (vgl. A_21217)
    # Das Zertifikat muss man vor der Verwendung prüfen, was 
    # hier nicht demonstriert wird (vgl. A_21216 oder A_21218).
    cert_der = f.read()
    cert = x509.load_der_x509_certificate(cert_der, default_backend())

    # Den öffentlichen Schlüssel benötigt man dann für die
    # ECIES-Verschlüsselung der Daten für die VAU (bspw. Anwendungsfall
    # E-Rezept einstellen).
    vau_public_key = cert.public_key()

    # Nur zur Info
    vau_pn = vau_public_key.public_numbers()
    logging.info("öffentlicher Langzeit-VAU-Schlüssel ({}-Kurve) x=0x{:x} y=0x{:x}".format(
                 vau_pn.curve.name, vau_pn.x, vau_pn.y))


# Als erstes erzeugte ich im Client einen ephemeres ECC-Schlüsselpaar.
# Solch ein Schlüsselpaar wird für jede Nachricht neu erzeugt.
private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
public_key = private_key.public_key()

# Nur zur Info
pn = public_key.public_numbers()
logging.info(f"erzeuge ephemeres Client-Schlüsselpaar ({pn.curve.name}-Kurve) x=0x{pn.x:x} y=0x{pn.y:x}" + \
             " d={:x}".format(private_key.private_numbers().private_value)
            )

logging.info("führe Elliptic-Curve-Diffie-Hellman durch")
SharedSecret = private_key.exchange(ec.ECDH(), vau_public_key)
logging.info("SharedSecret: {}".format(hexlify(SharedSecret).decode()))

# Nun leite ich einen AES-Schlüssel für AES/GCM aus dem gemeinsamen
# Geheimnis ab (SharedSecret).
hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None, 
            info=b'ecies-vau-transport', backend=default_backend()
)
aes_key = hkdf.derive(SharedSecret)
logging.info("AES-Key: " + hexlify(aes_key).decode())

plaintext = "Hallo Test".encode()
plaintext_len = len(plaintext)
print("Der Klartext ist:", plaintext.decode(),
      "\nKlartext-Hex:", hexlify(plaintext).decode(),
      "\nKlartext-Länge:", plaintext_len)

# Für jede Nachricht wird ein neuer IV zufällig erzeugt.
iv = secrets.token_bytes(12)
# alternativ könnte man auch 
#       iv = os.urandom(12) 
# machen. Hauptsache 12 Byte (= 96 Bit) Zufall.
print("zufällige Nounce (IV):", hexlify(iv).decode(), "(Länge: 12 Byte = 96 Bit, A_20161)")

# Die Nachricht wird jetzt mit AES/GCM verschlüsselt.
aesgcm = AESGCM(aes_key)
ciphertext = aesgcm.encrypt(iv, plaintext, associated_data=None)
print("AES/GCM-Ciphertext:",  hexlify(ciphertext).decode(),
      "(Länge:", len(ciphertext), "=", len(plaintext),
      "Byte Klartext-Länge + 16 Byte Authentication-Tag (GMAC))")
print("CT:", hexlify(ciphertext[:plaintext_len]).decode(), 
             hexlify(ciphertext[plaintext_len:]).decode())

x_str = "{:x}".format(pn.x).zfill(64)
assert len(x_str) == 64
y_str = "{:x}".format(pn.y).zfill(64)
assert len(x_str) == 64

# Das Chiffrat gemäß 
print("\"01\" Versionsnummer des Formats, 32 Byte X-Wert, 32 Byte Y-Wert, 12 Byte IV,"
      "{} AES/GCM-Ciphertext mit 16 Byte Authentication-Tag".format(len(plaintext)))
print("01", x_str, y_str, hexlify(iv).decode(), hexlify(ciphertext).decode())

print("Der zu übertragene Ciphertext nach A_20161-* ist damit (als hexdump dargestellt):\n",
      "01", x_str, y_str, hexlify(iv).decode(), hexlify(ciphertext).decode(), sep="")

# Nur zur Info
pt = aesgcm.decrypt(iv, ciphertext, associated_data=None)
print("Entschlüsselt:", pt.decode())

