# Die statuslose Variante des VAU-Protokolls für das E-Rezept

In dieserm Subtree findet man Code für die VAU-Protokoll-Variante für
das E-Rezept. 


## Verschlüsselung für die E-Rezept-VAU

Hier gibt es ein Beispiel für die Verschlüsselung der Requests für die VAU des
E-Rezept-Fachdienstes.


    [a@h erp]$ ./enc.py 
    [2021-03-02T09:56:20+0100] {/home/a/git/vau-protokoll/erp/./enc.py:33} INFO - öffentlicher Langzeit-VAU-Schlüssel (brainpoolP256r1-Kurve) x=0x8634212830dad457ca05305e6687134166b9c21a65ffebf555f4e75dfb048888 y=0x66e4b6843624cbda43c97ea89968bc41fd53576f82c03efa7d601b9facac2b29
    [2021-03-02T09:56:20+0100] {/home/a/git/vau-protokoll/erp/./enc.py:44} INFO - erzeuge ephemeres Client-Schlüsselpaar (brainpoolP256r1-Kurve) x=0x24b4e14ccd831d024d5f3f74edc39438193b99685f594171d7d63e9967317c2d y=0x8cc5328b8d53d4ea12c545fcc9ecb8d942d6fc6c6731a043bf59b298d97a367f d=54563cf6f09a93524dc37dcac43912452a4f4cb5e2c530cbdb0d91b44eac1635
    [2021-03-02T09:56:20+0100] {/home/a/git/vau-protokoll/erp/./enc.py:48} INFO - führe Elliptic-Curve-Diffie-Hellman durch
    [2021-03-02T09:56:20+0100] {/home/a/git/vau-protokoll/erp/./enc.py:50} INFO - SharedSecret: 5cb625e7f3995388f522ac905ec18750f220c0c5554beb96af368a7736df9871
    [2021-03-02T09:56:20+0100] {/home/a/git/vau-protokoll/erp/./enc.py:58} INFO - AES-Key: bab7d33578bda144a8eeb7fd6918e940
    Der Klartext ist: Hallo Test 
    Klartext-Hex: 48616c6c6f2054657374 
    Klartext-Länge: 10
    zufällige Nounce (IV): 68c36ff7be444336485ddcfb (Länge: 12 Byte = 96 Bit, A_20161)
    AES/GCM-Ciphertext: 1003c17b053269fec3eca42f53bfabb8cd91706b3395d6c10338 (Länge: 26 = 10 Byte Klartext-Länge + 16 Byte Authentication-Tag (GMAC))
    CT: 1003c17b053269fec3ec a42f53bfabb8cd91706b3395d6c10338
    "01" Versionsnummer des Formats, 32 Byte X-Wert, 32 Byte Y-Wert, 12 Byte IV,10 AES/GCM-Ciphertext mit 16 Byte Authentication-Tag
    01 24b4e14ccd831d024d5f3f74edc39438193b99685f594171d7d63e9967317c2d 8cc5328b8d53d4ea12c545fcc9ecb8d942d6fc6c6731a043bf59b298d97a367f 68c36ff7be444336485ddcfb 1003c17b053269fec3eca42f53bfabb8cd91706b3395d6c10338
    Der zu übertragene Ciphertext nach A_20161-* ist damit (als hexdump dargestellt):
    0124b4e14ccd831d024d5f3f74edc39438193b99685f594171d7d63e9967317c2d8cc5328b8d53d4ea12c545fcc9ecb8d942d6fc6c6731a043bf59b298d97a367f68c36ff7be444336485ddcfb1003c17b053269fec3eca42f53bfabb8cd91706b3395d6c10338
    Entschlüsselt: Hallo Test

Die Requests an die VAU sind natürlich nicht "Hallo Test", sondern
FHIR-JSON-Nachrichten wie in 
[https://github.com/gematik/ref-eRp-FD-Server/blob/master/server/examples/task\_create.plain](https://github.com/gematik/ref-eRp-FD-Server/blob/master/server/examples/task_create.plain)
aufgeführt.

Der Client erzeugt zunächst einen FHIR-Request als Datenstruktur (Textstring),
er fügt dann den entsprechenden HTTP-Request-Header (innererer Request /
Textstring) davor und anschließend

    "1" + Leerzeichen + Access-Token + Leerzeichen + Request-ID(hex) + Leerzeichen + AES-Antwort-Schlüssel(hex) + Leerzeichen

davor (vgl. A\_20161-\*). Das ist dann der Klartext, der wie in enc.py vorgeführt zu verschlüsseln ist.

## mal als Probe die Entschlüsselung in der VAU simulieren

    [a@h erp]$ ./vau-decrypt.py 
    Hexdump des ciphertext vom Nutzer für mich (VAU) ist:
    0124b4e14ccd831d024d5f3f74edc39438193b99685f594171d7d63e9967317c2d8cc5328b8d53d4ea12c545fcc9ecb8d942d6fc6c6731a043bf59b298d97a367f68c36ff7be444336485ddcfb1003c17b053269fec3eca42f53bfabb8cd91706b3395d6c10338
    [2021-03-06T18:18:10+0100] {/home/a/git/vau-protokoll/erp/./vau-decrypt.py:41} INFO - öffentlicher Langzeit-VAU-Schlüssel (brainpoolP256r1-Kurve) x=0x8634212830dad457ca05305e6687134166b9c21a65ffebf555f4e75dfb048888 y=0x66e4b6843624cbda43c97ea89968bc41fd53576f82c03efa7d601b9facac2b29
    [2021-03-06T18:18:10+0100] {/home/a/git/vau-protokoll/erp/./vau-decrypt.py:49} INFO - teste prefix 0x01
    [2021-03-06T18:18:10+0100] {/home/a/git/vau-protokoll/erp/./vau-decrypt.py:58} INFO - führe Elliptic-Curve-Diffie-Hellman durch
    [2021-03-06T18:18:10+0100] {/home/a/git/vau-protokoll/erp/./vau-decrypt.py:60} INFO - shared_secret: 5cb625e7f3995388f522ac905ec18750f220c0c5554beb96af368a7736df9871
    [2021-03-06T18:18:10+0100] {/home/a/git/vau-protokoll/erp/./vau-decrypt.py:68} INFO - Der Nachrichtenspezifische AES-Schlüssel ist bab7d33578bda144a8eeb7fd6918e940
    Entschlüsselt: Hallo Test

## Author
Andreas Hallof

