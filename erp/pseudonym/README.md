# Pseudonymerstellung für die Websocket-Schnittstelle für E-Rezept Eventing-Mechanismus Apothekeninformationssysteme

Der Code hier hat nichts mit dem VAU-Protokoll direkt zu tun. Ich hatte nur
keinen anderen Platz zum Parken des Codes.

    $ ./pseudonym.py
    tid=1-1-12345678-12345 psyeudonym=82a9a43c3ecb574a43a93c6367a27f62f3959acc46e07a94e8e88bd2866e4247
    tid=1-2-12345678-12345 psyeudonym=77e6cd7b86e79d875d44d386fefe05edca23a15a515417ceceec0538e5fc304a
    tid=2-1-12345678-12345 psyeudonym=046643b2ea9547696485caf9e4d8a0faeb3af6626c95caef0e4be8e2c762ce51

