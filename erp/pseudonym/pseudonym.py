#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Pseudonymerstellung für die Websocket-Signaling-Schnittstelle für die Apotheken
(AIS) 
"""

from binascii import hexlify

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF


# so das ist jetzt mal unserer im Wirkbetrieb min. 120 Bit Entropie haltiges
# Mastersecret
mastersecret = b'a'*256

def get_pseudonym(tid: str) -> str:
    """
        Erzeugt über die HKDF-SHA256 (vgl. [RFC-5869]) und ein
        systemweites und bspw. monatlich wechselndes Mastersecret 
        aus einer TID ein 256-Bit langes Pseudonym (hexkodierter 
        string).
    """

    hkdf = HKDF(algorithm=hashes.SHA256(), length=32, salt=None,
                info=tid.encode(), backend=default_backend())
    return hexlify(hkdf.derive(mastersecret)).decode()


if __name__ == '__main__':

    for tid in ['1-1-12345678-12345', '1-2-12345678-12345', '2-1-12345678-12345']:
        print("tid={} psyeudonym={}".format(tid, get_pseudonym(tid)))

    # für mehr Erläuterungen zur Telematik-ID vgl. gemSpec_PKI#Telematik-ID

