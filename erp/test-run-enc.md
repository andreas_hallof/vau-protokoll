# Testlauf für das Verschlüsselungsbeispiel

    [a@h erp]$ ./enc.py
    [2020-09-08T21:33:02+0200] {./enc.py:28} INFO - Langzeit-VAU-Client-Schlüssel (brainpoolP256r1-Kurve) x=0x8634212830dad457ca05305e6687134166b9c21a65ffebf555f4e75dfb048888 y=0x66e4b6843624cbda43c97ea89968bc41fd53576f82c03efa7d601b9facac2b29
    [2020-09-08T21:33:02+0200] {./enc.py:38} INFO - erzeuge ephemeres Client-Schlüsselpaar (brainpoolP256r1-Kurve) x=0xab68e9435dca456983930a62770461ac7f0c5e5dfc6d93032702e3213168248 y=0xa21e1df599ccd1832037101def5926069de865ee48bbc3ed92da273efe935cc7 d=83456d98dea3435c166385a4e644ebca588e8a0aa7c811f51fcc736368630206
    [2020-09-08T21:33:02+0200] {./enc.py:42} INFO - führe ECDH aus
    [2020-09-08T21:33:02+0200] {./enc.py:44} INFO - SharedSecret:
    29aaa0349b1a3aa99501ea0d087e05f0e7a51c356693be5ba010ca615c7bb5fd
    [2020-09-08T21:33:02+0200] {./enc.py:51} INFO - AES-Key: b912780c7e03fc579e6ada2f8491f1f5
    Der Klartext ist: b'Hallo Test'
    Klartext-Hex: b'48616c6c6f2054657374'
    Klartext-Länge: 10
    zufällige Nounce (IV): 5d91ddc679962d23d078ac77 (Länge: 12 Byte = 96 Bit, A_20161)
    AES/GCM-Ciphertext: d54a675fc7c4dd564af8a403f0da9a56eefd1026342f83b168ee (Länge: 26 = 10 Byte Klartext-Länge + 16 Byte Authentication-Tag (GMAC))
    CT: d54a675fc7c4dd564af8 a403f0da9a56eefd1026342f83b168ee
    "01" Versionsnummer des Formats, 32 Byte X-Wert, 32 Byte Y-Wert, 12 Byte IV,10 AES/GCM-Ciphertext mit 16 Byte Authentication-Tag
    01 0ab68e9435dca456983930a62770461ac7f0c5e5dfc6d93032702e3213168248 a21e1df599ccd1832037101def5926069de865ee48bbc3ed92da273efe935cc7 5d91ddc679962d23d078ac77 d54a675fc7c4dd564af8a403f0da9a56eefd1026342f83b168ee
    Entschlüsselt: b'Hallo Test


## das Beispiel für gemSpec\_Krypt 4.0.1

    [2020-08-12T12:32:22+0200] {./enc.py:27} INFO - Langzeit-VAU-Client-Schlüssel (brainpoolP256r1-Kurve) 
            x=0x8634212830dad457ca05305e6687134166b9c21a65ffebf555f4e75dfb048888 
            y=0x66e4b6843624cbda43c97ea89968bc41fd53576f82c03efa7d601b9facac2b29
    [2020-08-12T12:32:22+0200] {./enc.py:36} INFO - erzeuge ephemeres Client-Schlüsselpaar (brainpoolP256r1-Kurve) 
            x=0x754e548941e5cd073fed6d734578a484be9f0bbfa1b6fa3168ed7ffb22878f0f 
            y=0x9aef9bbd932a020d8828367bd080a3e72b36c41ee40c87253f9b1b0beb8371bf 
            d=5bbba34d47502bd588ed680dfa2309ca375eb7a35ddbbd67cc7f8b6b687a1c1d
    [2020-08-12T12:32:22+0200] {./enc.py:40} INFO - führe ECDH aus
    [2020-08-12T12:32:22+0200] {./enc.py:42} INFO - SharedSecret:
    9656c2b4b3da81d0385f6a1ee60e93b91828fd90231c923d53ce7bbbcd58ceaa
    [2020-08-12T12:32:22+0200] {./enc.py:49} INFO - AES-Key: 23977ba552a21363916af9b5147c83d4
    Länge plaintext 10 b'48616c6c6f2054657374'
    Nounce: 257db4604af8ae0dfced37ce
    Länge Ciphertext 26 86c2b491c7a8309e750b4e6e307219863938c204dfe85502ee0a
    CT: 86c2b491c7a8309e750b 4e6e307219863938c204dfe85502ee0a
    01 754e548941e5cd073fed6d734578a484be9f0bbfa1b6fa3168ed7ffb22878f0f 9aef9bbd932a020d8828367bd080a3e72b36c41ee40c87253f9b1b0beb8371bf 257db4604af8ae0dfced37ce 86c2b491c7a8309e750b4e6e307219863938c204dfe85502ee0a
    b'Hallo Test'
