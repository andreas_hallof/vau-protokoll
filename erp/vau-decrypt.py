#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Das hier ist ein Beispiel wie die VAU die Nachrichten eines Client
entschlüsseln kann. Das ist quasi die Probe, dass die Umkehrrichtung 
(also die Entschlüsselung) funktioniert.
"""

import logging
from binascii import hexlify, unhexlify

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
    datefmt="%Y-%m-%dT%H:%M:%S%z")

ciphertext_from_client_hex = """
0124b4e14ccd831d024d5f3f74edc39438193b99685f5941
71d7d63e9967317c2d8cc5328b8d53d4ea12c545fcc9ecb8d942d6fc6c6731a043bf59b29
8d97a367f68c36ff7be444336485ddcfb1003c17b053269fec3eca42f53bfabb8cd91706b
3395d6c10338""".replace("\n","")
ciphertext_from_client = unhexlify(ciphertext_from_client_hex)
# So, diesen Bytestrom habe ich als Ciphertext vom Client bekommen.
print("Hexdump des ciphertext vom Nutzer für mich (VAU) ist:\n",
    hexlify(ciphertext_from_client).decode(), sep='')

# Den pivaten Laufzeit-VAU-Schlüssel laden (beim E-Rezept-FD wäre dieser
# im HSM).
with open("pki/vau_key.pem", "rb") as f:
    vau_private_key = serialization.load_pem_private_key(
        f.read(), password=None, backend=default_backend())

# Nur zur Info
vau_pn = vau_private_key.public_key().public_numbers()
logging.info("öffentlicher Langzeit-VAU-Schlüssel ({}-Kurve) x=0x{:x} y=0x{:x}".format(
                vau_pn.curve.name, vau_pn.x, vau_pn.y))


assert len(ciphertext_from_client) > 1 + 32 + 32 + 12 + 16
# Natürlich werden die Nachrichten schon aufgrund der enthaltenen 
# Access-Token (zwei mal) schon deutlich größer sein müssen.

logging.info("teste prefix 0x01")
assert ciphertext_from_client[0] == 1

pub_numbers = ec.EllipticCurvePublicNumbers(
    int.from_bytes(ciphertext_from_client[1:1+32], byteorder='big'),
    int.from_bytes(ciphertext_from_client[33:33+32], byteorder='big'),
    ec.BrainpoolP256R1())
message_public_key = pub_numbers.public_key()

logging.info("führe Elliptic-Curve-Diffie-Hellman durch")
shared_secret = vau_private_key.exchange(ec.ECDH(), message_public_key)
logging.info("shared_secret: {}".format(hexlify(shared_secret).decode()))

# Nun leite ich einen AES-Schlüssel für AES/GCM aus dem gemeinsamen
# ECDH-Geheimnis ab.
hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None, 
            info=b'ecies-vau-transport', backend=default_backend()
)
aes_key = hkdf.derive(shared_secret)
logging.info("Der Nachrichtenspezifische AES-Schlüssel ist " + hexlify(aes_key).decode())


# Die Nachricht wird jetzt mit AES/GCM entchlüsselt.
aesgcm = AESGCM(aes_key)
iv = ciphertext_from_client[1+32+32:1+32+32+12]
assert len(iv) == 12
ciphertext = ciphertext_from_client[1+32+32+12:]
pt = aesgcm.decrypt(iv, ciphertext, associated_data=None)
print("Entschlüsselt:", pt.decode())

