#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os
from binascii import hexlify

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

with open("pki/vau_cert.der", "rb") as f:
    cert = x509.load_der_x509_certificate(f.read(), default_backend())
    vau_public_key = cert.public_key()

# für jede Nachricht neu zu erzeugen:
private_key = ec.generate_private_key(ec.BrainpoolP256R1(), default_backend())
pn = private_key.public_key().public_numbers()
shared_secret = private_key.exchange(ec.ECDH(), vau_public_key)
hkdf = HKDF(algorithm=hashes.SHA256(), length=16, salt=None, info=b'ecies-vau-transport', backend=default_backend())
aes_key = hkdf.derive(shared_secret)
plaintext = "Hallo Test".encode()
iv = os.urandom(12)
ciphertext = AESGCM(aes_key).encrypt(iv, plaintext, associated_data=None)
x_str = "{:x}".format(pn.x).zfill(64)
y_str = "{:x}".format(pn.y).zfill(64)

print("Der zu übertragene Ciphertext nach A_20161-* ist damit (als hexdump dargestellt):\n",
      "01", x_str, y_str, hexlify(iv).decode(), hexlify(ciphertext).decode(), sep="")

