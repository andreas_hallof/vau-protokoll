#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import time

def post_req_encode(url: str, mimetype: str, data: bytes):

    assert url
    assert mimetype
    assert data

    mimetype = mimetype.lower()
    if mimetype == "cbor":
        mimetype = "application/cbor"
    elif mimetype == "octet":
        mimetype = "application/octet-stream"
    else:
        raise ValueError()

    m = """POST {} HTTP/1.1
Host: epa-aktensystem.de
Context-Length: {}
Context-Type: application

""".format(url, len(data)).encode()

    return m + data

def post_resp_encode(mimetype: str, data: bytes):

    assert mimetype
    assert data

    mimetype = mimetype.lower()
    if mimetype == "cbor":
        mimetype = "application/cbor"
    elif mimetype == "octet":
        mimetype = "application/octet-stream"
    else:
        raise ValueError()

    m = """200 HTTP/1.1
Context-Length: {}
Context-Type: {}

""".format(len(data), mimetype).encode()

    return m + data

