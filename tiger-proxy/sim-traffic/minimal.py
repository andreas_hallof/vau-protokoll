#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Ich erzeuge Test-Traffic für den Test der Triger-Proxy-Funktionalität
VAU-Protokoll in Nichtproduktivumgebungen analyiseren (entschlüsseln)
zu können.
"""



# für das Laden der privaten Schlüssels der VAU
# aus der Datei vau_server_keys.cbor
from cryptography.hazmat.primitives import serialization

# debugging
from icecream import ic

# real stuff
import encvau, hashlib, kem, time, cbor2, trafficdump, httpencodings

if __name__ == '__main__':

    ic.configureOutput(argToStringFunction=kem.toString)
    t = trafficdump.trafficdumper("dump1.tiger")

    #
    # Client: nachricht-1 VAUClientHello
    #
    ic("Beginn Erzeugung von Nachricht 1")
    client_schluessel_1 = kem.gen_keypairs()

    nachricht_1 = {
            "MessageType" : "M1",
            "ECDH_PK"    : client_schluessel_1["ECDH"]["pub_key"],
            "Kyber768_PK" : client_schluessel_1["Kyber768"]["pub_key"]
            }

    nachricht_1_encoded = cbor2.dumps(nachricht_1)
    transscript_client = nachricht_1_encoded

    t.write("c2s", httpencodings.post_req_encode("/VAU", "cbor", nachricht_1_encoded))

    #
    # Server: nachricht-2 VAUServerHello
    #
    ic("Beginn Erzeugung von Nachricht 2")
    with open("vau_server_signed_pub_keys.cbor", "rb") as pk_file:
        vau_server_signed_pub_keys = pk_file.read()

    transscript_server = nachricht_1_encoded
    server_kem_result_1 = kem.encapsulation(nachricht_1)
    (S_K1_s2c, S_K1_c2s) = kem.kem_kdf(server_kem_result_1)
    aead_ciphertext_msg_2 = kem.AEAD_enc(
            S_K1_s2c, vau_server_signed_pub_keys)

    nachricht_2 = {
            "MessageType" : "M2",
            "ECDH_ct"    : server_kem_result_1["ECDH_ct"],
            "Kyber768_ct" : server_kem_result_1["Kyber768_ct"],
            "AEAD_ct"     : aead_ciphertext_msg_2
            }

    nachricht_2_encoded = cbor2.dumps(nachricht_2)
    transscript_server += nachricht_2_encoded
    t.write("s2c", httpencodings.post_resp_encode("cbor", nachricht_2_encoded))

    #
    # Client: nachricht-3 VAUClientKEM+KeyConfirmation
    #
    ic("Beginn Erzeugung von Nachricht 3")
    transscript_client += nachricht_2_encoded
    client_kem_result_1 = kem.decapsulation(nachricht_2, client_schluessel_1)
    ic("Schlüsselableitung für die K1-Schlüssel")
    (C_K1_s2c, C_K1_c2s) = kem.kem_kdf(client_kem_result_1)
    assert C_K1_s2c == S_K1_s2c
    assert C_K1_c2s == S_K1_c2s
    transfered_signed_vau_server_pub_keys = kem.AEAD_dec(
            C_K1_s2c, nachricht_2["AEAD_ct"])
    # Im Produktivcode try-Umgebung
    signed_vau_server_pub_keys = cbor2.loads(transfered_signed_vau_server_pub_keys)
    # Zertifikate signed_vau_server_pub_keys["cert"] prüfen
    # Signatur signed_vau_server_pub_keys["signature-ES256"] prüfen
    pub_keys = cbor2.loads(signed_vau_server_pub_keys["signed_pub_keys"])
    # prüfen von pub_key["exp"]
    client_kem_result_2 = kem.encapsulation(pub_keys)
    # ERP = Enforce Replay Protection
    # ESO = Enforce Sequence Order
    nachricht_3_inner_layer = {
            "ECDH_ct"    : client_kem_result_2["ECDH_ct"],
            "Kyber768_ct" : client_kem_result_2["Kyber768_ct"],
            "ERP" : False,
            "ESO" : False
    }
    nachricht_3_inner_layer_encoded = cbor2.dumps(nachricht_3_inner_layer)
    aead_ciphertext_msg_3 = kem.AEAD_enc(C_K1_c2s, nachricht_3_inner_layer_encoded)
    transscript_client_to_send = transscript_client + aead_ciphertext_msg_3

    ic("Schlüsselableitung für die K2-Schlüssel")
    (C_K2_c2s_KeyConfirmation, C_K2_c2s_AppData, C_K2_s2c_KeyConfirmation,
     C_K2_s2c_AppData, C_KeyID) = kem.kem_kdf(client_kem_result_1, client_kem_result_2)
    transscript_client_hash = hashlib.sha256(transscript_client_to_send).digest()
    aead_ciphertext_msg_3_key_confirmation = \
        kem.AEAD_enc(C_K2_c2s_KeyConfirmation, transscript_client_hash)

    nachricht_3 = {
            "MessageType" : "M3",
            "AEAD_ct"     : aead_ciphertext_msg_3,
            "AEAD_ct_key_confirmation" : aead_ciphertext_msg_3_key_confirmation
            }

    nachricht_3_encoded = cbor2.dumps(nachricht_3)
    transscript_client += nachricht_3_encoded
    t.write("c2s", httpencodings.post_req_encode("/VAU", "cbor", nachricht_3_encoded))

    #
    # Server: nachricht-4 VAUServerKeyConfirmation
    #
    ic("Beginn Erzeugung von Nachricht 4")
    transscript_server_to_check = transscript_server + nachricht_3["AEAD_ct"]
    transscript_server += nachricht_3_encoded

    # Im Produktivcode try-Umgebung
    kem_cts = kem.AEAD_dec(S_K1_c2s, nachricht_3["AEAD_ct"])
    kem_cts = cbor2.loads(kem_cts)
    # Im Produktivcode würden die VAU-Server-Schlüssel schon in der 
    # VAU-Instanz einsatzbereit liegen, weil Sie zuvor dort auch erzeugt wurden
    # vgl. A_24427
    with open("vau_server_keys.cbor", "rb") as vau_server_keys:
        tmp = vau_server_keys.read()
        tmp2 = cbor2.loads(tmp)
        vau_server_keys = {
                "ECDH"    : { "priv_key" : \
                    serialization.load_der_private_key(tmp2["ECDH_PrivKey"],
                                                       password=None) },
                "Kyber768" : { "priv_key" : tmp2["Kyber768_PrivKey"]}
                }

    server_kem_result_2 = kem.decapsulation(kem_cts, vau_server_keys)
    (S_K2_c2s_KeyConfirmation, S_K2_c2s_AppData, S_K2_s2c_KeyConfirmation,
     S_K2_s2c_AppData, S_KeyID) = kem.kem_kdf(server_kem_result_1, server_kem_result_2)

    # Im Produktivcode try-Umgebung
    transscript_hash_des_clients = kem.AEAD_dec(
                            S_K2_c2s_KeyConfirmation,
                            nachricht_3["AEAD_ct_key_confirmation"])

    vau_hash_berechnung_client_transscript_hash = hashlib.sha256(
                                    transscript_server_to_check).digest()

    # Gleichheit der Hashwerte prüfen (KeyConfirmation)
    assert transscript_hash_des_clients == vau_hash_berechnung_client_transscript_hash

    transscript_server_hash = hashlib.sha256(transscript_server).digest()
    aead_ciphertext_msg_4_key_confirmation = kem.AEAD_enc(
            S_K2_s2c_KeyConfirmation, transscript_server_hash)

    nachricht_4 = {
            "MessageType" : "M4",
            "AEAD_ct_key_confirmation" : aead_ciphertext_msg_4_key_confirmation
            }

    nachricht_4_encoded = cbor2.dumps(nachricht_4)
    t.write("s2c", httpencodings.post_resp_encode("cbor", nachricht_4_encoded))

    # Auswertung von Nachricht 4 im Client (KeyConfirmation checken)

    # Im Produktivcode try-Umgebung
    transscript_von_der_vau = kem.AEAD_dec(C_K2_s2c_KeyConfirmation,
                                           nachricht_4["AEAD_ct_key_confirmation"])

    transscript_client_hash = hashlib.sha256(transscript_client).digest()

    # Gleichheit der Hashwerte prüfen (KeyConfirmation)
    assert transscript_von_der_vau == transscript_client_hash

    client = encvau.VAUProtokollKeyPair(
                encvau.VAUProtokollContext("client", False),
                C_K2_c2s_AppData, C_K2_s2c_AppData, C_KeyID)

    server = encvau.VAUProtokollKeyPair(
                encvau.VAUProtokollContext("server", False),
                S_K2_c2s_AppData, S_K2_s2c_AppData, S_KeyID)

    mytext = "Hallo Welt!"
    ct_req_1 = client.send(mytext)
    pt = server.recv(ct_req_1)
    mytext_2 = ic(pt.decode())
    assert mytext == mytext_2

    mytext = "Antwort der Welt"
    ct_resp_1 = server.send(mytext)
    pt = client.recv(ct_resp_1)
    mytext_2 = ic(pt.decode())

