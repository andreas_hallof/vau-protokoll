#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import time

class trafficdumper():
    def __init__(self, filename, nonPU=True):
        assert filename
        self.my_file = open(filename, "wb")
        self.magic_bytes = b"TIGER-PROXY-DUMP"

    def write(self, direction: str, data: bytes):
        assert len(direction.encode())==3
        assert len(data)>0

        header = (self.magic_bytes +
                  direction.encode() +
                  time.time_ns().to_bytes(signed=False, length=8, byteorder="big") +
                  len(data).to_bytes(signed=False, length=8, byteorder="big"))

        self.my_file.write(header + data)

