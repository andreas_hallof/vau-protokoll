#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Die beiden VAUServer-Schlüsselpaare erzeugen.  Diese werden von einer VAU
erzeugt und dann über die SIG-Identität der VAU (deren privater Schlüssel sich
in dem VAU-HSM befindet) signiert.

"""

from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature, InvalidTag
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

from base64 import b64encode
from binascii import hexlify
from icecream import ic
import cbor2, kem, time

# Los geht's

if __name__ == '__main__':

    ic.configureOutput(argToStringFunction=kem.toString)
    vau_server_schlüssel = kem.gen_keypairs()

    # Diese beiden Schlüsselpaar werden in einer VAU-Instanz erzeugt
    # und die öffentlichen Schlüssel über die im HSM befindliche 
    # SIG-Identität signiert. Das Ergebnis ist dann 
    # "signierte öffentliche VAU-Schlüssel" (A_24425)
    vau_server_keys = {
            "ECDH_PK"       : vau_server_schlüssel["ECDH"]["pub_key"],
            "ECDH_PrivKey"  : \
                    # Die genau Wahl der Kodierung hier irrelevant,
                    # weil nur VAU-intern verwendet.
                vau_server_schlüssel["ECDH"]["priv_key"].private_bytes(
                         encoding=serialization.Encoding.DER,
                         format=serialization.PrivateFormat.PKCS8,
                         encryption_algorithm=serialization.NoEncryption()),
            "Kyber768_PK"    : vau_server_schlüssel["Kyber768"]["pub_key"],
            "Kyber768_PrivKey" : vau_server_schlüssel["Kyber768"]["priv_key"]
            }

    ic(vau_server_keys)

    with open("vau_server_keys.cbor", "wb") as server_keys_file:
        server_keys_file.write(cbor2.dumps(vau_server_keys))

    vau_server_pub_keys = {
            "ECDH_PK"       : vau_server_keys["ECDH_PK"],
            "Kyber768_PK"    : vau_server_keys["Kyber768_PK"],
            "iat" : int(time.time()),
            "exp" : int(time.time())+24*60*60*7,
            "comment" : "Erzeugt bei VAU-Instanz xyz, Meta-Info abcd"
            }
    vpks_encoded = cbor2.dumps(vau_server_pub_keys)
    with open("../pki/vau-sig-key.pem", "rb") as priv_key_file:
        vau_sign_priv_key = serialization.load_pem_private_key(priv_key_file.read(), password=None)
    signature = vau_sign_priv_key.sign(vpks_encoded, ec.ECDSA(hashes.SHA256()))
    with open("../pki/vau_sig_cert.der", "rb") as sig_cert_file:
        vau_sig_cert = sig_cert_file.read()
    with open("../pki/ocsp-response-vau-sig.der", "rb") as vau_sig_cert_ocsp_file:
        vau_sig_cert_ocsp_response = vau_sig_cert_ocsp_file.read()

    result = {"signed_pub_keys" : vpks_encoded,
              "signature-ES256" : signature,
              "cert"            : vau_sig_cert,
              "ocsp_response"   : vau_sig_cert_ocsp_response}
    with open("vau_server_signed_pub_keys.cbor", "wb") as sspks:
        sspks.write(ic(cbor2.dumps(result)))

