#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

import cbor2, secrets

import oqs
import oqs.rand as oqsrand

from icecream import ic
from binascii import hexlify

def cbor_encode_ecc_pub_key(ecc_public_key: ec.EllipticCurvePublicKey):
    """
    Verständnishinweis:
    Bei ECDH entspricht (in unserem Kontext)
    ein ECC-Punkt einem öffentlichen Schlüssel = einem Chiffrat.
    """

    pub_numbers = ecc_public_key.public_numbers()
    ecdh_ct = cbor2.dumps({"crv": "P-256",
                            "x": pub_numbers.x.to_bytes(
                                    length=32, byteorder='big',
                                    signed=False),
                            "y": pub_numbers.y.to_bytes(
                                    length=32, byteorder='big',
                                    signed=False)
                            })
    return ecdh_ct


def cbor_decode_ecc_pub_key(data: bytes):
    # Im Produktiv-Code muss das in einer try-Umgebung laufen,
    # ... invalid encoding etc.
    ecc_public_key_decoded = cbor2.loads(data)
    assert ecc_public_key_decoded["crv"] == "P-256"
    assert len(ecc_public_key_decoded["x"])==32
    assert len(ecc_public_key_decoded["y"])==32

    public_numbers = ec.EllipticCurvePublicNumbers(
            int.from_bytes(ecc_public_key_decoded["x"], byteorder='big', signed=False),
            int.from_bytes(ecc_public_key_decoded["y"], byteorder='big', signed=False),
            ec.SECP256R1())
    result = public_numbers.public_key()

    return result

def gen_keypairs():
    """
    xxx
    """

    ecdh_private_key = ec.generate_private_key(ec.SECP256R1())
    ecdh_public_key = ecdh_private_key.public_key()
    ecdh_public_key_encoded = cbor_encode_ecc_pub_key(ecdh_public_key)

    with oqs.KeyEncapsulation("Kyber768") as pqc_client:
        #oqsrand.randombytes_switch_algorithm("OpenSSL")
        #print('{:17s}'.format("OpenSSL:"), ' '.join('{:02X}'.format(x) for x in oqsrand.randombytes(32)))

        #oqsrand.randombytes_nist_kat_init_256bit(bytes(os.urandom(48)))
        #oqsrand.randombytes_switch_algorithm("NIST-KAT")
        pqc_public_key = pqc_client.generate_keypair()

        result = { "ECDH"    : {"pub_key"  : ecdh_public_key_encoded,
                                 "priv_key" : ecdh_private_key},
                   "Kyber768" : {"pub_key"  : pqc_public_key,
                                 "priv_key" : pqc_client.export_secret_key()}
                 }

    return result

def encapsulation(pk_keys)-> list:
    """
    xxx
    """

    remote_ecc_public_key = cbor_decode_ecc_pub_key(pk_keys["ECDH_PK"])
    tmp_private_key = ec.generate_private_key(ec.SECP256R1())
    ecdh_ct = cbor_encode_ecc_pub_key(tmp_private_key.public_key())
    ecdh_shared_secret = tmp_private_key.exchange(ec.ECDH(), remote_ecc_public_key)

    ic(ecdh_shared_secret)

    with oqs.KeyEncapsulation("Kyber768") as server:
        Kyber768_ct, Kyber768_shared_secret = server.encap_secret(pk_keys["Kyber768_PK"])
        ic(Kyber768_shared_secret)

    return {"ECDH_ct": ecdh_ct,
            "ECDH_ss": ecdh_shared_secret,
            "Kyber768_ct": Kyber768_ct,
            "Kyber768_ss": Kyber768_shared_secret}

def kem_kdf(kem_result_1, kem_result_2 = None):
    """
    xxx
    """
    assert len(kem_result_1["ECDH_ss"])>0
    assert len(kem_result_1["Kyber768_ss"])>0

    if kem_result_2:
        shared_secret = kem_result_1["ECDH_ss"] + \
                        kem_result_1["Kyber768_ss"] + \
                        kem_result_2["ECDH_ss"] + \
                        kem_result_2["Kyber768_ss"]
        target_len = 5*32 # 4 256-Bit-AES-Schlüssel + ein 256-Bit KeyID
    else:
        shared_secret = kem_result_1["ECDH_ss"] + kem_result_1["Kyber768_ss"]
        target_len = 2*32 # 2 256-Bit-AES-Schlüssel

    tmp = HKDF(algorithm=hashes.SHA256(), length=target_len, salt=None,
               info=b'').derive(shared_secret)

    result = []
    for i in range(0, len(tmp) >> 5):
        result.append(tmp[i*32:(i+1)*32])

    return result

def AEAD_enc(key, plaintext):
    assert len(key)==32
    assert len(plaintext)>0

    iv = secrets.token_bytes(12)
    ciphertext = AESGCM(key).encrypt(iv, plaintext, associated_data=None)

    return iv + ciphertext

def AEAD_dec(key, ciphertext):
    assert len(key)==32
    assert len(ciphertext)>0

    iv = ciphertext[:12]
    ct = ciphertext[12:]
    # Im Code für eine Produktiv-Umgebung muss man die Entschlüsselung
    # in einer try-Umgebung (exceptions) durchführen.
    plaintext = AESGCM(key).decrypt(iv, ct, associated_data=None)

    return plaintext

def decapsulation(ciphertexts, priv_keys):
    assert isinstance(ciphertexts, dict)
    assert isinstance(priv_keys, dict)

    ecc_public_key_sender = cbor_decode_ecc_pub_key(ciphertexts["ECDH_ct"])
    ecdh_shared_secret = priv_keys["ECDH"]["priv_key"].exchange(ec.ECDH(), ecc_public_key_sender)

    ic(ecdh_shared_secret)

    #ic(priv_keys["Kyber768"]["priv_key"])
    with oqs.KeyEncapsulation("Kyber768", priv_keys["Kyber768"]["priv_key"]) as client:
        shared_secret_client = client.decap_secret(ciphertexts["Kyber768_ct"])
        ic(shared_secret_client)

    return {"ECDH_ss" : ecdh_shared_secret,
            "Kyber768_ss" : shared_secret_client}


# Debugging-Ausgabe hübscher machen:
def toString(obj):
    if isinstance(obj, dict):
        new_dict = dict()
        for i in obj:
            if isinstance(obj[i], bytes) or isinstance(obj, bytearray):
                new_dict[i] = hexlify(obj[i]).decode()
            else:
                new_dict[i] = obj[i]
        return repr(new_dict)

    if isinstance(obj, bytes):
        return hexlify(obj).decode()

    return repr(obj)


if __name__ == '__main__':
    print("Shall be imported, and not be call directly.")
