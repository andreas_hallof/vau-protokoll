Was funktioniert:

- LibOQS [https://github.com/open-quantum-safe/liboqs-python](https://github.com/open-quantum-safe/liboqs-python)
- Plugin für OpenSSL [https://github.com/open-quantum-safe/oqs-provider](https://github.com/open-quantum-safe/oqs-provider)
- Botan3 [https://botan.randombit.net/handbook/index.html](https://botan.randombit.net/handbook/index.html)

