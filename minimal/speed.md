# >5 Jahre alter Dell-Laptop Latitdue 7480

     processor	: 0
     vendor_id	: GenuineIntel
     cpu family	: 6
     model	: 78
     model name	: Intel(R) Core(TM) i7-6650U CPU @ 2.20GHz


     $ for i in $(seq 1 10) ; do ./minimal.py 2>/dev/null; done | grep Time
     Time-Total: 0.08429956436157227
     Time-Total: 0.08329916000366211
     Time-Total: 0.08319377899169922
     Time-Total: 0.08270430564880371
     Time-Total: 0.08304715156555176
     Time-Total: 0.08305001258850098
     Time-Total: 0.08309364318847656
     Time-Total: 0.08379602432250977
     Time-Total: 0.08501148223876953
     Time-Total: 0.08433961868286133

# virtuelle Linux-Maschine in der "Cloud"  (tipkg.de)

    processor	: 0
    vendor_id	: GenuineIntel
    cpu family	: 6
    model	: 45
    model name	: Intel(R) Xeon(R) CPU E5-2670 0 @ 2.60GHz
    stepping	: 7

    $ for i in $(seq 1 10) ; do ./minimal.py 2>/dev/null; done | grep Time
    Time-Total: 0.14638495445251465
    Time-Total: 0.1516590118408203
    Time-Total: 0.1488654613494873
    Time-Total: 0.1442887783050537
    Time-Total: 0.14173078536987305
    Time-Total: 0.15921759605407715
    Time-Total: 0.14046835899353027
    Time-Total: 0.14605474472045898
    Time-Total: 0.15330862998962402
    Time-Total: 0.15555715560913086


# 15 Jahre alter Linux PC

    processor       : 3
    vendor_id       : GenuineIntel
    cpu family      : 6
    model           : 23
    model name      : Intel(R) Core(TM)2 Quad CPU    Q9550  @ 2.83GHz

    a@t15:~/git/vau-protokoll/minimal$ for i in $(seq 1 10); do ./minimal.py 2>&1 |grep Time-Total; done
    Time-Total für die Handshake-Phase: 0.18897581100463867
    Time-Total für die Handshake-Phase: 0.19012689590454102
    Time-Total für die Handshake-Phase: 0.18936920166015625
    Time-Total für die Handshake-Phase: 0.18918919563293457
    Time-Total für die Handshake-Phase: 0.18850493431091309
    Time-Total für die Handshake-Phase: 0.18858838081359863
    Time-Total für die Handshake-Phase: 0.19081664085388184
    Time-Total für die Handshake-Phase: 0.18855667114257812
    Time-Total für die Handshake-Phase: 0.1890866756439209
    Time-Total für die Handshake-Phase: 0.18869805335998535

