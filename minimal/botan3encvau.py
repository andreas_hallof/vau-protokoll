#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Der Teil des VAU-Protokolls, der nach dem Handshake kommt.
(Nutzdaten ver-/entschlüsseln)

Mittels Botan3-Bibliothek.

"""

# für die Zufallszahlen
import secrets
# AES/GCM
import botan3


class VAUProtokollContext:
    """
    Die Rolle eines Teilnehmers im VAU-Protokoll kann entweder client oder
    server sein.

    Wichtig ist auch ob, die Kommunikation in der PU oder Nichtproduktiv-
    Umgebung stattfindet.
    """
    def __init__(self, role="client", pu=False):
        assert role in ["client", "server"]
        self.role=role
        assert isinstance(pu, bool)
        self.pu=pu

class VAUProtokollKeyPair():
    def __init__(self, context: VAUProtokollContext,
                 K2_c2s_app_data, K2_s2c_app_data, KeyID):
        self.context = context
        assert len(K2_c2s_app_data) == 32
        assert len(K2_s2c_app_data) == 32
        assert len(KeyID) == 32
        self.K2_c2s_app_data = K2_c2s_app_data
        self.K2_s2c_app_data = K2_s2c_app_data
        self.KeyID = KeyID
        self.request_counter = 0
        self.encryption_counter = 0
        # xxx

    def send(self, plaintext):

        assert len(plaintext)>0
        if isinstance(plaintext, str):
            plaintext = plaintext.encode()

        if self.context.role == "client":
            mykey = self.K2_c2s_app_data
        else:
            mykey = self.K2_s2c_app_data

        VersionByte = b'\2'
        PUByte = b'\1' if self.context.pu else b'\0'
        if self.context.role == "client":
            self.request_counter += 1
            req_byte = b'\1'
        else:
            req_byte = b'\2'
        req_ctr_bytes = self.request_counter.to_bytes(length=8, byteorder="big", signed=False)
        key_id = self.KeyID

        header = VersionByte + PUByte + req_byte + req_ctr_bytes + key_id

        # Botan 3
        my_cipher = botan3.SymmetricCipher("AES-256/GCM")
        my_cipher.set_key(mykey)
        my_cipher.set_assoc_data(header)
        iv = secrets.token_bytes(12)
        my_cipher.start(iv)
        ciphertext = my_cipher.finish(plaintext)

        # openssl:
        # # iv = secrets.token_bytes(12)
        # # ciphertext = AESGCM(mykey).encrypt(iv, plaintext, associated_data=header)

        result = header + iv + ciphertext

        return result

    def recv(self, ciphertext):

        assert len(ciphertext)>1+1+1+8+32+12+1+16
        assert isinstance(ciphertext, bytes)

        if self.context.role == "client":
            mykey = self.K2_s2c_app_data
        else:
            mykey = self.K2_c2s_app_data

        header = ciphertext[0:43]
        VersionByte = header[0]
        assert VersionByte == 2
        PUByte = header[1]
        assert PUByte == (1 if self.context.pu else 0)
        req_byte = header[2]
        if self.context.role == "client":
            assert req_byte == 2
        else:
            assert req_byte == 1
        req_ctr = int.from_bytes(header[3:3+8], byteorder='big', signed=False)

        if self.context.role == "client":
            assert self.request_counter == req_ctr
        else:
            # Annahme ERP und ESO sind beide auf False, muss ich als
            # VAU also (im in der aktuellen Projektphase) nicht durchsetzen
            self.request_counter = req_ctr

        key_id = header[11:]
        assert len(key_id) == 32

        iv = ciphertext[43:43+12]
        ct = ciphertext[55:]

        # Im Code für eine Produktiv-Umgebung muss man die Entschlüsselung
        # in einer try-Umgebung (exceptions) durchführen.

        # Botan 3
        my_cipher = botan3.SymmetricCipher("AES-256/GCM", encrypt=False)
        my_cipher.set_key(mykey)
        my_cipher.set_assoc_data(header)
        my_cipher.start(iv)
        plaintext = my_cipher.finish(ct)

        # # openssl
        # plaintext = AESGCM(mykey).decrypt(iv, ct, associated_data=header)

        return plaintext

if __name__ == '__main__':
    print("Shall be imported, and not be call directly.")

