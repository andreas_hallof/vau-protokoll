#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import botan3, cbor2, secrets, sys

from icecream import ic
from binascii import hexlify, unhexlify

from typing import TypedDict

ecc_pk_dict = TypedDict("ecc_pk_dict", { "crv": str, "x": bytes, "y": bytes})

def encode_ecc_pk(ecc_public_key: botan3.PublicKey):
    """
    Verständnishinweis:
    Bei ECDH entspricht (in unserem Kontext) ein ECC-Punkt einem öffentlichen
    Schlüssel = einem Chiffrat.

    Als Eingabe-Argument bekomme ich hier von Botan3 den DER-Kodierten
    ECC-Schlüssel. Ich extrahiere die x- und y-Koordinaten.
    """

    #ecc_public_key_der = ecc_public_key.to_der()

    #ASN1_PREFIX = unhexlify("3057301106052b8104010c06082a8648ce3d030107034200")

    #assert len(ecc_public_key_der) == len(ASN1_PREFIX) + 1 + 32 + 32
    #assert ecc_public_key_der.startswith(ASN1_PREFIX + b'\x04')
    ## Prefix und '\x04' wird abgeschnitten
    #tmp = ecc_public_key_der[len(ASN1_PREFIX)+1:]; assert len(tmp) == 64
    #x = tmp[:32]; assert len(x) == 32
    #y = tmp[32:]; assert len(y) == 32

    tmp = ecc_public_key.get_public_point()
    tmp = tmp[1:]
    x = tmp[:32]; assert len(x) == 32
    y = tmp[32:]; assert len(y) == 32

    ecdh_ct = {"crv": "P-256", "x" : x, "y": y}

    return ecdh_ct


def decode_ecc_pk(data: ecc_pk_dict)-> bytes:
    """
    mit dem dekodierten öffentlichen Schlüssel möchte ich ECDH machen.
    Dafür muss ein Schlüssel bei botan im Format nach TR-03111#3.2.1
    vorliegen. Von der API-Wahl etwas merkwürdig, bei openssl/python-
    crytography übergibt man ein öffentlichens Schüssel-Objekt.
    Aber egal, wie auch immer.
    """
    assert data["crv"] == "P-256"
    assert len(data["x"])==32
    assert len(data["y"])==32

    # Im Produktivcode muss das auch in einer try-Umgebung laufen:
    # prüfen ob der Punkt auf der Kurve liegt etc.
    ecdh_pk_encoded = b'\x04' + data["x"] + data["y"]

    #    ecdh_pk = botan3.PublicKey().load_ecdh(
    #             "secp256r1",
    #             int.from_bytes(data['x'], byteorder='big', signed=False),
    #             int.from_bytes(data['y'], byteorder='big', signed=False))
    #
    return ecdh_pk_encoded

def gen_keypairs():
    """
    Ich erzeuge zwei Schlüsselpaare: eines für ECDH und eines für Kyber-768
    """

    random_source = botan3.RandomNumberGenerator(rng_type='system')
    ecdh_prk = botan3.PrivateKey.create("ecdh", "secp256r1", random_source)
    ecdh_pk = ecdh_prk.get_public_key()
    ecdh_pk_encoded = encode_ecc_pk(ecdh_pk)

    pqc_prk = botan3.PrivateKey.create("Kyber", "Kyber-768-r3", random_source)
    #pqc_pk_encoded  = pqc_prk.get_public_key().to_der()
    #Assert len(pqc_pk_encoded) == 1184 + 24
    #Assert pqc_pk_encoded.startswith(unhexlify("308204b4300d060b2b0601040181c52a010702038204a100"))
    #Pqc_pk_encoded = pqc_pk_encoded[24:]
    pqc_pk_encoded  = pqc_prk.get_public_key().view_kyber_raw_key()
    assert len(pqc_pk_encoded) == 1184

    result = { "ECDH"     : {"pub_key"  : ecdh_pk_encoded,
                             "priv_key" : ecdh_prk},
               "Kyber768" : {"pub_key"  : pqc_pk_encoded,
                             "priv_key" : pqc_prk}
             }

    return result

def encapsulation(pk_keys)-> list:
    """
    xxx
    """

    remote_ecc_public_key = decode_ecc_pk(pk_keys["ECDH_PK"])
    random_source = botan3.RandomNumberGenerator(rng_type='system')
    tmp_ecc_prk_key = botan3.PrivateKey.create("ecdh", "secp256r1", random_source)
    k_a = botan3.PKKeyAgreement(tmp_ecc_prk_key, "Raw")
    ecdh_shared_secret = k_a.agree(remote_ecc_public_key, 32, b"")
    tmp_pk = tmp_ecc_prk_key.get_public_key()
    ecdh_ct = encode_ecc_pk(tmp_pk)

    ic(ecdh_shared_secret)
    ic(ecdh_ct)

    #ic(pk_keys["Kyber768_PK"])
    public_key = botan3.PublicKey().load_kyber(pk_keys["Kyber768_PK"])
    enc = botan3.KemEncrypt(public_key, "Raw")
    (Kyber768_shared_secret, Kyber768_ct) = enc.create_shared_key(random_source,
                                                                 b"", 32)
    #ic(Kyber768_shared_secret)
    #ic(Kyber768_ct)

    return {"ECDH_ct": ecdh_ct,
            "ECDH_ss": ecdh_shared_secret,
            "Kyber768_ct": Kyber768_ct,
            "Kyber768_ss": Kyber768_shared_secret}

def kem_kdf(kem_result_1, kem_result_2 = None):
    """
    xxx
    """
    assert len(kem_result_1["ECDH_ss"])>0
    assert len(kem_result_1["Kyber768_ss"])>0

    if kem_result_2:
        shared_secret = kem_result_1["ECDH_ss"] + \
                        kem_result_1["Kyber768_ss"] + \
                        kem_result_2["ECDH_ss"] + \
                        kem_result_2["Kyber768_ss"]
        target_len = 5*32 # 4 256-Bit-AES-Schlüssel + ein 256-Bit KeyID
    else:
        shared_secret = kem_result_1["ECDH_ss"] + kem_result_1["Kyber768_ss"]
        target_len = 2*32 # 2 256-Bit-AES-Schlüssel

    tmp = botan3.kdf("HKDF(SHA-256)", shared_secret, target_len, b"", b"")

    result = []
    for i in range(0, len(tmp) >> 5):
        result.append(tmp[i*32:(i+1)*32])

    return result

def AEAD_enc(key, plaintext):
    assert len(key)==32
    assert len(plaintext)>0

    iv = secrets.token_bytes(12)
    #openssl:
    #ciphertext = AESGCM(key).encrypt(iv, plaintext, associated_data=None)
    #botan3
    my_cipher = botan3.SymmetricCipher("AES-256/GCM", encrypt=True)
    my_cipher.set_key(key)
    my_cipher.start(iv)
    ciphertext = my_cipher.finish(plaintext)

    return iv + ciphertext

def AEAD_dec(key, ciphertext):
    assert len(key)==32
    assert len(ciphertext)>0

    iv = ciphertext[:12]
    ct = ciphertext[12:]
    # Im Code für eine Produktiv-Umgebung muss man die Entschlüsselung
    # in einer try-Umgebung (exceptions) durchführen.
    #openssl:
    #plaintext = AESGCM(key).decrypt(iv, ct, associated_data=None)
    #botan3
    my_cipher = botan3.SymmetricCipher("AES-256/GCM", encrypt=False)
    my_cipher.set_key(key)
    my_cipher.start(iv)
    # hier kann eine Exception kommen! muss man im Produktiv-Code
    # entsprechend behandelt xxx
    plaintext = my_cipher.finish(ct)


    return plaintext

def decapsulation(ciphertexts: dict, priv_keys: dict):

    ecc_pk_sender = decode_ecc_pk(ciphertexts["ECDH_ct"])

    #print(priv_keys)
    #sys.exit(1)
    k_a = botan3.PKKeyAgreement(priv_keys["ECDH"]["priv_key"], "Raw")
    ecdh_shared_secret = k_a.agree(ecc_pk_sender, 32, b"")

    dec = botan3.KemDecrypt(priv_keys["Kyber768"]["priv_key"], "Raw")
    kyber_shared_secret = dec.decrypt_shared_key(\
                                salt=b'', desired_key_len=32,\
                                encapsulated_key=ciphertexts["Kyber768_ct"])

    return {"ECDH_ss" : ecdh_shared_secret,
            "Kyber768_ss" : kyber_shared_secret}


# Debugging-Ausgabe hübscher machen:
def toString(obj):
    if isinstance(obj, dict):
        new_dict = dict()
        for i in obj:
            if isinstance(obj[i], bytes) or isinstance(obj, bytearray):
                new_dict[i] = "(hexdump L={}) ".format(len(obj[i])) \
                                + hexlify(obj[i]).decode()
            elif isinstance(obj[i], dict):
                tmp = dict()
                for my_i in obj[i]:
                    if isinstance(obj[i][my_i], bytes) or isinstance(obj, bytearray):
                        tmp[my_i] = "(hexdump L={}) ".format(len(obj[i][my_i]))\
                                + hexlify(obj[i][my_i]).decode()
                    else:
                        tmp[my_i] = obj[i][my_i]
                new_dict[i]=tmp
            else:
                new_dict[i] = obj[i]
        return json.dumps(new_dict, indent=4)

    if isinstance(obj, bytes):
        return "(hexdump L={}) ".format(len(obj)) + hexlify(obj).decode()

    return repr(obj)


if __name__ == '__main__':
    print("Shall be imported, and not be call directly.")

